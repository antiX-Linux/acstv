#!/bin/bash
# -*- coding: utf-8 -*-
# antiX Community Simpel TV starter script (aCSTV)
# Version 1.13 Revision A
# GPL v.3
# Written 10/2021 by Robin for antiX community
# questions, suggestions and bug reporting please to www.antixforum.com
# This program was originally written in German language.
# English and other foreign language users please download the latest version of translation files (.mo type)
# and put them into the respective system folder in order to see the user interface of aCSTV in your language.
# 11/2022 update, fit for antiX 22, and improved language file and stations list management.
# 02/2023 update (ver 1.09) fix: replacement for buggy 'xdotool search --name' function not working reliable for feh windows.
# 05/2023 update (ver 1.10) added protection against environments not assigning separate pgid to aCSTV when starting. Fit for antiX 23
# 01/2024 update (ver 1.11, 1.12) added fix for wrong stream selection by mpv when multiple audio streams are transferred by a station.
#         (revision A and B) impoved stations list update functionality.
#         update (ver 1.13) improved handling of exorbitant stations lists: reading in chunks for accellerated processing, allowing to
#         drop stations lists limits and warnings.

# expects: feh, socat, buffer, mpv, yd-dlp, sed, xdotool, wmctrl, wget, bc, fonts-freefont-ttf, gtkdialog, yad, netcat-traditional, gettext
# expects to be able to write tempfiles to /dev/shm
# expects to find gtk named icons "aCSTV" and "aCSTV2"

# todo: Benutzername/Paßwort für Streamingdienste, per URL (add setup for user accounts for paid streaming services)
# todo: Mediatheken der Sender auf sub-buttons (am besten umschalt+button) legen (add mediathek function for senders on shift+button or right clock on button if possible)
# todo: Unterstützung für Untertitel einbauen (add support for subtitle function)
# todo: Gtkdialogskript für das Hauptfenster verschlanken: Redundanzen in der Tastensteuerung beseitigen (betrifft Kommunikation zwischen gtkdialog action tags). Ein Hauptfenster
#       mit 287 Tasten erzeugt z.B. derzeit eine sinnlos große, (über 6MB, 90.000 Zeilen) Temporärscriptdatei, da jeder Steuerbefehl für alle anderen Tasten im action tag jeder Taste
#       wiederholt werden muß. Evtl. erlaubt die aktuelle gtkdialog-Version, die in antiX 22 enthalten ist, eine verbesserte Kommunikation zwischen den action tags.

# known bugs: button of sender running isn't displayed as »pressed« (greyed out) after a record was stopped. This is an optical problem merely, not a functional since
# (redundant) code was added to prevent the button to execute a channel change anyway in case the very program is running already. If somebody finds a way
# to make gtkdlg respect a command expansion within <action type=...> tags the specific button number could get greyed out again (and the code could be written much leaner also).
# known bug: (probably fixed by --ytdl-raw-options=abort-on-error=) mpv doesn't accept any new station when previous station had failed to connect under some conditions, even if idle-active status is properly returned.

TEXTDOMAINDIR=/usr/share/locale
TEXTDOMAIN=aCSTV.sh
version="1.13a"

# Schutz vor Umgebungen, die aCSTV keine eigene separate Prozessgruppe zuweisen. (Protection against environments not assigning a separate pgid to aCSTV when starting)
[ "$$" != "$(ps -o pgid= "$$" 2>/dev/null | grep -o "[[:digit:]]*")" ] && exec setsid "$(readlink -f "$0")" "$@"

# Sonstige konfigurierbare Voreinstellungen: (Configurable variables) ------------------------------
bibliothekspfad="/usr/local/lib/aCSTV"                                  # Pfad zu aCSTV Ressourcen (Path to aCSTV resources)
konfigurationspfad="$HOME/.config/aCSTV"                                # Pfad zu den Konfigurationsdateien von aCSTV (Path to aCSTV config files)
einstellungsdatei=$"aCSTV-Einstellungen"".lst"                          # Name der Einstellungsdatei, es wird eine einfache Textdatei mit einer beliebigen Erweiterung erwartet. (Path to settings file, simple text file expected with arbitrary extension)
stationsliste=$"aCSTV-Sender"".lst"                                     # Basisname der Senderliste, es wird eine einfache Textdatei mit einer beliebigen Erweiterung erwartet.  (Base name for stations lists, simple text file expected with arbitrary extension)
anleitung=$"Bedienungsanleitung"".pdf"                                  # Basisname der Bedienungsanleitung. Es wird eine pdf-Datei erwartet. (Base name of user manual, pdf file expected)
export ip_fuer_verbindungspruefung="8.8.8.8 443"                        # Hier für die Internetverbindungsprüfung eine verläßliche IP Serveradresse mit Port getrennt durch ein Leerzeichen, OHNE DOPPELPUNKT dazwischen, eintragen. (URL for connection check. Expects reliable server address followed by port number separated by a single blank, NO DOUBLE POINT CHARACTER in between.
export aktualisierung="https://github.com/iptv-org/iptv/tree/master/streams"  # Kompatible Adresse für die Aktualisierung der Stationslisten (compatible URL for updating stations lists files)
symbol_aCSTV="aCSTV"                                                    # has to be an ›named icon‹ known by gtk rather than a path to an icon file here.
symbol_einstellungen="aCSTV"
symbol_internet="aCSTV"
symbol_installation="aCSTV"
symbol_fehler="aCSTV"
symbol_aufnahme="aCSTV2"
symbol_sprachauswahl="aCSTV"
#symbol_einstellungen="/usr/share/icons/Adwaita/24x24/apps/system-software-install.png"   # Statusleistensymbol für Einstellungsdialog for antiX 17/19
#[ ! -e "$symbol_einstellungen" ] && symbol_einstellungen="/usr/share/icons/Adwaita/24x24/legacy/system-software-install.png"   #for antiX 21/22
#symbol_internet="/usr/share/icons/Adwaita/24x24/status/network-error.png"                # Statusleistensymbol für fehlende Internetverbindung antiX 17/19
#[ ! -e "$symbol_internet" ] && symbol_internet="/usr/share/icons/Adwaita/24x24/legacy/network-error.png"   #for antiX 21/22
#symbol_installation="/usr/share/icons/Adwaita/24x24/apps/system-software-install.png"    # Statusleistensymbol für socat Installationsdialog for antiX 17/19
#[ ! -e "$symbol_installation" ] && symbol_installation="/usr/share/icons/Adwaita/24x24/legacy/system-software-install.png"   #for antiX 21/22
#---------------------------------------------------------------------------------------------------

export fs=""
export stationsliste_nutzer=""
export GtkdlgRahmen="false"
export YadRahmen="--undecorated"
export MpvRahmen="--no-border"
export FehRahmen="-x"

# (Command line help)
function Hilfe {
    echo -e "\n  "$"aCSTV — antiX Community Simple TV                             Ver."" $version""\n\n  "$"Dieses Script bietet eine intuitive und einfach zu bedienende Benutzer-""\n  "\
$"oberfläche  zur Wiedergabe  regionaler oder  international  verfügbarer""\n  "$"Fernsehsender als Internet Live-Stream. Die Wiedergabe von Live-Streams"\
"\n  "$"setzt eine bestehende  Internetverbindung voraus.  Eine Mitschnitt- und""\n  "$"eine  Szenenfotofunktion sowie Schnellzugriff auf eine Programmvorschau""\n  "\
$"im Webbrowser ergänzen den Funktionsumfang. Die Wiedergabe erfolgt wahl-""\n  "$"weise über den  primären PC-Bildschirm oder ein anderes an den Computer"\
"\n  "$"angeschlossenes  Anzeigegerät  wie z.B. ein  Fernseher.  Alle Parameter""\n  "$"sind  frei konfigurierbar.  Weitere Informationen  in der Programmhilfe""\n  "\
$"bei den »Einstellungen«.""\n\n  "$"Befehlszeilenoptionen:""\n\n  "$"--Hilfe oder -h . . . . . . . . . . . . . . . Diese Kurzhilfe anzeigen."\
"\n  "$"--Senderliste oder -s <Datei> . . . . . Datei als Senderliste verwenden." \
"\n  "$"--Rahmen oder -r  . . . . . . . . . . . Fensterrahmen anzeigen." \
"\n\n  "$"Fragen, Anregungen und Fehlermeldungen bitte an:""\n\n\t<www.antixforum.com>\n"
# entry needed to comply with EU legislation. This GPL text may not get translated. See gnu.org for details.
echo -e "  Copyright 2021, 2022, 2023, 2024 the antiX comunity\n\n  This program is free software: you can redistribute it and/or modify\n  it under the terms of the GNU General Public License as published by\n  "\
"the  Free Software Foundation, either  version 3  of the License, or\n  (at your option) any later version.\n\n  This program is distributed in the hope that it will be useful,\n  "\
"but  WITHOUT ANY WARRANTY; without even the implied warranty of\n  MERCHANTABILITY  or  FITNESS FOR A PARTICULAR PURPOSE.  See the\n  GNU General Public License for more details.\n\n  "\
"You should have received a copy of the GNU General Public\n  License along with this program.  If not, see\n\n\t<http://www.gnu.org/licenses/>.\n"
}

# Befehlszeilenoptionen einlesen (read command line options)
Befehlszeilenoptionen=$(getopt -o hs:r -l "help",$"Hilfe",$"Rahmen",$"Senderliste": -n "$(basename -s "" "$0")" -- "$@")
# String position: Command line error message in case program wasn't able to start properly
if [ $? != 0 ]; then echo $"Abbruch" >&2; exit 1; fi
eval set -- "$Befehlszeilenoptionen"

# Befehlszeilenoptionen auswerten (evaluate command line options)
while true; do
    case "$1" in
            # String position: Command line option -u (translatable long form). Important: Use a single word WITHOUT BLANKS when translating, but you may hyphenate some words together (e.g. this-is-a-long-option)
            -r|--$"Rahmen")
               export GtkdlgRahmen="true"
               export YadRahmen=""
               export MpvRahmen=""
               export FehRahmen=""
               shift
            ;;
            # String position: Command line option -h (translatable long form). Important: Use a single word WITHOUT BLANKS when translating, but you may hyphenate some words together (e.g. this-is-a-long-option)
            -h|--$"Hilfe"|--"help") Hilfe && exit 0; shift;;
	        # String position: Command line option -s (translatable long form). Important: Use a single word WITHOUT BLANKS when translating, but you may hyphenate some words together (e.g. this-is-a-long-option)
	        -s|--$"Senderliste") :; shift; stationsliste_nutzer="$1"; shift;;
            --) shift; break ;;
	        # String position: Command line error message if an invalid argument was filed on command line.
	        *) echo $"Ungültige Befehlszeilenoption:"" -${OPTARG}."\
                     $"Bitte rufen Sie""  $(basename -s "" "$0") "\
                     $"mit der Option"" -h "$"oder"" --"$"Hilfe"" "$"auf."
	           shift
	           exit 1
	        ;;
		esac
	done

if [ -n "$stationsliste_nutzer" ] && [ ! -f "$stationsliste_nutzer" ]; then
   echo -e "$(basename -s "" "$0"): "$"Datei nicht vorhanden:"\
   "\n        »$stationsliste_nutzer«"\
   "\n        "$"Bitte geben Sie mit"" -s "$"oder"' --'$"Senderliste"' '$"eine gültige"\
   "\n        "$"Stationslistendatei an."
   exit 1
fi

# Temporärdateien vorbereiten (prepare tempfiles)
temporaerdatei_01="/dev/shm/aCSTV-$$01.tmp"               # enthält den Haupt-gtkdialog (contains gtk main dialog)
export temporaerdatei_02="/dev/shm/aCSTV-$$02.tmp"        # pid der verwendeten mpv Instanz (contains pid of mpv instance used)
export temporaerdatei_03="/dev/shm/aCSTV-$$03.tmp"        # enthält die Funktion zur Grundeinstellung. (contains function for basic settings) Behelfskonstruktion für die nichtfunktionalen exportierten Funktionen in gtkdialog (Workaround for not functional exproted functions in gtkdialog)
export temporaerdatei_04="/dev/shm/aCSTV-$$04.tmp"        # enthält die URL des laufenden Senders. (Contains URL of station running). Teil der Behelfskonstruktion für den nichtfunktionalen mpv ipv Befehl [echo '{ "command": ["stream-record", "/path/file"] }' | socat - $mpvsocket] (Part of the workaround for nonfunctional MPV IPV command "stream-record")
export temporaerdatei_05="/dev/shm/aCSTV-$$05.tmp"        # Flag-Datei, existiert während einer Aufnahme, (flag file, signalling "record". Workaround for...) Teil der Behelfskonstruktion für den nichtfunktionalen mpv ipv Befehl [echo '{ "command": ["stream-record", "/path/file"] }' | socat - $mpvsocket]
export temporaerdatei_06="/dev/shm/aCSTV-$$06.tmp"        # Startscript für die Aufnahmefunktion, (Contains Script for record function. Workaround for...) Behelfskonstruktion für die nichtfunktionalen exportierten Funktionen in gtkdialog
export temporaerdatei_07="/dev/shm/aCSTV-$$07.tmp"        # Stopscript für Aufnahmefunktion, (Contains Script for stopping a running record. Workaround for...) Behelfskonstruktion für die nichtfunktionalen exportierten Funktionen in gtkdialog
export temporaerdatei_08="/dev/shm/aCSTV-$$08.tmp"        # enthält den Namen des laufenden Senders, (Contains Name of Station playing. Workaround for...) Teil der Behelfskonstruktion für den nichtfunktionalen mpv ipv Befehl [echo '{ "command": ["stream-record", "/path/file"] }' | socat - $mpvsocket]
export temporaerdatei_09="/dev/shm/aCSTV-$$09.tmp"        # Hilfskonstruktion für die Anzeige der Programmhilfe (Workaround for display of program help function)
export temporaerdatei_10="/dev/shm/aCSTV-$$10.tmp"        # enthält die Nummer des Programmplatzes des laufenden Senders, (Contains Number of Station running, Workaround for...). Teil der Behelfskonstruktion für den nichtfunktionalen mpv ipv Befehl [echo '{ "command": ["stream-record", "/path/file"] }' | socat - $mpvsocket]
export temporaerdatei_11="/dev/shm/aCSTV-$$11.tmp"        # Flag-Datei, existiert während der Wiedergabe. (flag file, signalling playing. Workaround for...) Teil der Behelfskonstruktion für den nichtfunktionalen mpv ipv Befehl [echo '{ "command": ["stream-record", "/path/file"] }' | socat - $mpvsocket]
export temporaerdatei_12="/dev/shm/aCSTV-$$12.tmp"        # enthält die Funktion für die Bearbeitung der Einträge in der Stationsliste. (Contains function for editing station list file)
export temporaerdatei_13="/dev/shm/aCSTV-$$13.tmp"        # enthält während der Bearbeitng der Stationsliste die jeweils aktuelle Anzahl an Einträgen (contains the recent number of entries in station list while editing)
export temporaerdatei_14="/dev/shm/aCSTV-$$14.tmp"        # Arbeitskopie der Senderliste während deren Bearbeitung (working copy of stations list while editing)
export temporaerdatei_15="/dev/shm/aCSTV-$$15.tmp"        # enthält die Funktion zur automatischen Aktualisierung der Senderliste per Internet. (contains function for automatically updating the stations list from internet)
export temporaerdatei_16="/dev/shm/aCSTV-$$16.tmp"        # Arbeitskopie der Senderliste während deren Bearbeitung (working copy of stations list while editing)
export temporaerdatei_17="/dev/shm/aCSTV-$$17.tmp"        # Arbeitskopie der Senderliste während deren Bearbeitung (working copy of stations list while editing)
export temporaerdatei_18="/dev/shm/aCSTV-$$18.tmp"        # Script zum Wechseln der Senderliste aus dem Hauptdialog (contains script for changing stations list file from main dialog)
export temporaerdatei_19="/dev/shm/aCSTV-$$19.tmp"        # Script zur Information über laufende Aufnahme
export temporaerdatei_22="/dev/shm/aCSTV-$$22.tmp"        # Temporärdatei für Stationslistenaktualisierung (temp file for automatic stations list update)
export fifo_warteschlange_01="/dev/shm/aCSTV-fifo-$$01.tmp" # Fifo-Warteschlange für die Bearbeitung der Stationsliste (fifo queue for editing stations list file)
export fifo_warteschlange_02="/dev/shm/aCSTV-fifo-$$02.tmp" # Fifo-Warteschlange für die Statusmeldung beim Aktualisieren der der Stationsliste (fifo queue for updating stations list file)
export fifo_warteschlange_03="/dev/shm/aCSTV-fifo-$$03.tmp" # Fifo-Warteschlange für die Übermittlung von Befehlen an yad (fifo yad command queue)
export mpvsocket="/dev/shm/aCSTV-mpv-socket-$$00"         # Schnittestelle für die Kommunikation mit MPV. (socket for MPV communication and command transfer)
flag_retry=true

# Aufräumen bei Verlassen des Programms vorbereiten (prepare clean exit)
# Sicherstellen, daß beim Verlassen des Programms keine verwaisten Prozesse oder Temporärdateien übrigbleiben. (make sure there are no orphan processes or tempfiles left on exit)
function aufräumen {
echo "stop" | socat - "$mpvsocket"
sleep .5
echo "quit" | socat - "$mpvsocket"
[ -e "$HOME/.config/aCSTV/mpv/input.conf" ] && rm -f "$HOME/.config/aCSTV/mpv/input.conf"    # Zurücksetzen der mpv Konfiguration
[ -e "$temporaerdatei_01" ] && rm -f "$temporaerdatei_01"
[ -e "$temporaerdatei_02" ] && rm -f "$temporaerdatei_02"
[ -e "$temporaerdatei_03" ] && rm -f "$temporaerdatei_03"
[ -e "$temporaerdatei_04" ] && rm -f "$temporaerdatei_04"
[ -e "$temporaerdatei_05" ] && rm -f "$temporaerdatei_05"
[ -e "$temporaerdatei_06" ] && rm -f "$temporaerdatei_06"
[ -e "$temporaerdatei_07" ] && rm -f "$temporaerdatei_07"
[ -e "$temporaerdatei_08" ] && rm -f "$temporaerdatei_08"
[ -e "$temporaerdatei_09" ] && rm -f "$temporaerdatei_09"
[ -e "$temporaerdatei_10" ] && rm -f "$temporaerdatei_10"
[ -e "$temporaerdatei_11" ] && rm -f "$temporaerdatei_11"
[ -e "$temporaerdatei_12" ] && rm -f "$temporaerdatei_12"
[ -e "$temporaerdatei_13" ] && rm -f "$temporaerdatei_13"
[ -e "$temporaerdatei_15" ] && rm -f "$temporaerdatei_15"
[ -e "$temporaerdatei_16" ] && rm -f "$temporaerdatei_16"
[ -e "$temporaerdatei_17" ] && rm -f "$temporaerdatei_17"
[ -e "$temporaerdatei_18" ] && rm -f "$temporaerdatei_18"
[ -e "$temporaerdatei_19" ] && rm -f "$temporaerdatei_19"
[ -e "$temporaerdatei_22" ] && rm -f "$temporaerdatei_22"
[ -e "$fifo_warteschlange_01" ] && rm -f "$fifo_warteschlange_01"
[ -e "$fifo_warteschlange_02" ] && rm -f "$fifo_warteschlange_02"
[ -e "$fifo_warteschlange_03" ] && rm -f "$fifo_warteschlange_03"
if xdotool search --name $"aCSTV antiX Community Simple TV Starter" > /dev/null; then        # Schließen noch evtl. geöffneter gtkdialog Programmfenster
    xdotool windowclose $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
fi
if pgrep -f "/dev/shm/aCSTV-$$" > /dev/null; then kill -15 $(pgrep -f /dev/shm/aCSTV-$$) 2>/dev/null; fi # abschießen aller nicht folgsamen gtkdialog Unterprozesse von aCSTV, die beim Schließen des Dialogfensters sonst einfach weiterlaufen.
while pgrep -f '/dev/shm/aCSTV-$$' > /dev/null; do sleep .5 ; done # Warten, bis die Prozesse wirklich beendet sind, sonst öffnet sich die Senderliste immer wieder erneut.
if xdotool search --name $"aCSTV Senderliste verwalten" > /dev/null; then   # Senderliste schließen, falls sie beim Verlassen des Programms noch geöffnet ist
    yadpid="$(xdotool getwindowpid $(xdotool search --name $"aCSTV Senderliste verwalten")) " # und damit nicht nur das Fenster geschlossen wird, sondern yad auch wirklich beendet wird
    xdotool windowclose $(xdotool search --name $"aCSTV Senderliste verwalten")        # braucht es tatsächlich zwei Befehle. Erst das Fenster schließen.
    kill -15 "$yadpid" 2>/dev/null                                                    # Und dann yad, braucht eine Extraeinladung zum Beenden.
fi
if xdotool search --name $"aCSTV Einstellungen verwalten" > /dev/null; then  # Einstellungsfenster schließen, falls es beim Verlassen des Programms noch geöffnet ist
    yadpid="$(xdotool getwindowpid $(xdotool search --name $"aCSTV Einstellungen verwalten"))"
    xdotool windowclose $(xdotool search --name $"aCSTV Einstellungen verwalten")
    kill -15 "$yadpid" 2>/dev/null
fi
rm -f "$mpvsocket" 2>/dev/null # hinter mpv herräumen. Es läßt den socket, den es anlegt immer herumliegen.
[ -e "$temporaerdatei_14" ] && rm -f "$temporaerdatei_14"                            # this has to be the last file to be removed in case of iregular program termination.
#echo "Mischief managed. Leaving"  # marker for cleanup having been executed properly on leaving.
kill 0
}
trap aufräumen EXIT

# Spracheinstellungen (locale settings)
ui_lang_4="$(locale | grep ^LANG= | cut -d= -f2 | cut -d. -f1)"        # don't use system variable $LANG here, it causes an "rev: stdin: Invalid or incomplete multibyte or wide character" error in many languages for some reason.
ui_lang_2="$(locale | grep ^LANG= | cut -d= -f2 | cut -d_ -f1)"
ui_land=$(cut -d_ -f2 <<<"$ui_lang_4"); ui_land=${ui_land,,}

export einstellungsdatei="$(sed "s/\(.*\)\.\(.*\)/\1.$ui_lang_4.\2/" <<< "$konfigurationspfad/$einstellungsdatei")"
export anleitung="$(sed "s/\(.*\)\.\(.*\)/\1.$ui_lang_4.\2/" <<< "$konfigurationspfad/$anleitung")"
if [ -z "$stationsliste_nutzer" ]; then
    export stationsliste="$(sed "s/\(.*\)\.\(.*\)/\1.$ui_lang_4.\2/" <<< "$konfigurationspfad/$stationsliste")"
else
    export stationsliste="$stationsliste_nutzer"
fi

# Konfigurationsverzeichnis anlegen, falls es noch fehlt (create configuration directory if not present already)
mkdir -p "$konfigurationspfad"

# Verknüpfung aus dem Home-Verzeichnis des Nutzers zur betriebssystemweiten Stationslistenbibliothek (alle Nutzer) anlegen. (create symbolic link from user's home to system wide stations lists library)
if ! [ -h "$konfigurationspfad""/"$"Senderlisten-Bibliothek" ]; then
    [ -e "$konfigurationspfad""/"$"Senderlisten-Bibliothek" ] || [ -d  "$konfigurationspfad""/"$"Senderlisten-Bibliothek" ] && mv "$konfigurationspfad""/"$"Senderlisten-Bibliothek" "$konfigurationspfad""/"$"Senderlisten-Bibliothek"".bak"
    ln -s "$bibliothekspfad""/Stationslisten"  "$konfigurationspfad""/"$"Senderlisten-Bibliothek"
fi

# Modifiziertes Lua-hook-skript für MPV kopieren
cp -r "$bibliothekspfad/mpv" "$konfigurationspfad"

# Für den Neustart nach Bearbeiten der Senderliste warten, bis die vorhergehende Instanz mit schreiben der Dateien fertig ist. (Wait on restart of whole script after editing stations list file until all settings files from instance before are written before really starting up)
i=0; while [ "$(pgrep -f /dev/shm/aCSTV-$$)" != "$(pgrep -f /dev/shm/aCSTV-)" ]; do sleep .5; let $((i++)); [ $i -ge 60 ] && exit 1 ; done

# Yad Befehlswarteschlange anlegen
[ -p "$fifo_warteschlange_03" ] || mkfifo "$fifo_warteschlange_03"

function tastenbefehle {
# Reagieren auf Kurzwahltasten aktivieren. Die gewählten Tasten sind in mpv nicht belegt. (make mpv aware of some extra shortcuts from aCSTV. These are not predefined in mpv by now)
# This must be done on each run of aCSTV again, since the command has to contain current PID of this very aCSTV instance.
mkdir -p "$HOME/.config/aCSTV/mpv"
echo '- run "/bin/bash" "-c" "if xdotool search --onlyvisible --name '$(printf "\x27")$"aCSTV antiX Community Simple TV Starter"$(printf "\x27")'; then xdotool search --name '$(printf "\x27")$"aCSTV antiX Community Simple TV Starter"$(printf "\x27")' windowminimize; else xdotool search --name '$(printf "\x27")$"aCSTV antiX Community Simple TV Starter"$(printf "\x27")' windowmap; fi"' > ""$HOME"/.config/aCSTV/mpv/input.conf"
echo 'b run "/bin/bash" "-c" "if xdotool search  --name '$(printf "\x27")$"aCSTV antiX Community Simple TV Starter"$(printf "\x27")'; then xdotool search --name '$(printf "\x27")$"aCSTV antiX Community Simple TV Starter"$(printf "\x27")' windowclose; fi; kill -15 '$$'"' >> ""$HOME"/.config/aCSTV/mpv/input.conf"
}

function ländereinstellung {
# Multilinguale Umgebung verwalten (manage multilanguage environment)
    if ! [ -e "$anleitung" ]; then
        anleitung_land="$bibliothekspfad/Handbücher/$ui_lang_4"'.pdf'
        if [ -e "$anleitung_land" ]; then
            cp "$anleitung_land" "$anleitung"
        else
            anleitung_basis="$bibliothekspfad/Handbücher/$ui_lang_2"'.pdf'
            if [ -e "$anleitung_basis" ]; then
                cp "$anleitung_basis" "$anleitung"
            else
                $(ls "$bibliothekspfad"'/Handbücher/'*'.pdf' > /dev/null)
                if [ $? -eq 0 ]; then
                    for datei in "$bibliothekspfad"'/Handbücher/'*'.pdf'; do
                        k=$(rev <<< "$datei" | cut -d. -f2- | cut -d/ -f1 | rev | sed 's#^\(..*\)\.\(..*\)$#\2#')
                        l=""; m=""
                        l=$(locale -a | grep -m1 "$k")
                        if [ -n "$(grep "_" <<< "$k")" ]; then
                            m=$(LANG="$l" locale -ck LC_IDENTIFICATION | grep "^title=" | cut -d= -f2 | tr -d '"' | sed -n 's/^\(..*\)locale for \(..*\)$/\1 (\2)/p')
                        else  m=$(LANG="$l" locale -ck LC_IDENTIFICATION | grep "^title=" | cut -d= -f2 | tr -d '"' | sed -n 's/^\(..*\)locale for \(..*\)$/\1/p')
                        fi
                        if [ -n "$m" ]; then
                            sprachenliste_anleitung="$sprachenliste_anleitung$m („$k”)\n"
                        else
                            sprachenliste_anleitung="$sprachenliste_anleitung„$k”\n"
                        fi
                    done
                        sprachenliste_anleitung="$(echo -en "$sprachenliste_anleitung" | sort | tr "\n" "|" | tr -s '|' | rev | cut -c2- | rev)"
                    while [ "$manuelle_auswahl" == "" ]; do
                        manuelle_auswahl="$(yad --center --fixed $YadRahmen --borders=10 \
                        --window-icon="$symbol_sprachauswahl" \
                        --title=$"aCSTV — Fehler" \
                        --text="<b>"$"Die Bedienungsanleitung für <i>aCSTV</i> wurde noch nicht in Ihre Sprache übersetzt.""</b>\n\n"$"Bitte wählen Sie eine andere Sprache aus, in der die Programmhilfe angezeigt werden wird.""\n"  \
                        --form --separator="|" --item-separator="|" \
                        --field=$"Verfügbare Sprachen:":CB "$sprachenliste_anleitung" \
                        --button=$"Auswahl akzeptieren":6)"
                        tastenauswahl=$?
                        if [ $tastenauswahl == 6 ]; then
                            manuelle_auswahl="$(sed -n 's/^.*„\(..*\)”.*$/\1/p' <<< "$manuelle_auswahl")"
                            break
                        fi
                    done
                    anleitung_manuell="$bibliothekspfad/Handbücher/$manuelle_auswahl"'.pdf'
                else
                    anleitung_manuell="none"
                fi
                if [ -e "$anleitung_manuell" ]; then
                    cp "$anleitung_manuell" "$anleitung"
                else
                    echo $"This should never happen!"" (296) "$"Please make sure script was installed properly."
                fi
            fi
        fi
    fi
    if ! [ -e "$einstellungsdatei" ]; then
        einstellungsdatei_land="$bibliothekspfad/Einstellungen/$ui_lang_4"'.lst'
        if [ -e "$einstellungsdatei_land" ]; then
            cp "$einstellungsdatei_land" "$einstellungsdatei"
        else
            einstellungsdatei_basis="$bibliothekspfad/Einstellungen/$ui_lang_2"'.lst'
            if [ -e "$einstellungsdatei_basis" ]; then
                cp "$einstellungsdatei_basis" "$einstellungsdatei"
            else
                einstellungsdatei_manuell="$bibliothekspfad/Einstellungen/$manuelle_auswahl"'.lst'
                if [ -e "$einstellungsdatei_manuell" ]; then
                    cp "$einstellungsdatei_manuell" "$einstellungsdatei"
                else
                    cat <<< "kbps='2999'
bildschirm='0'
programminformationen='"$"https://www.klack.de/fernsehprogramm/alles-auf-einen-blick/-1/free.html""'
standbildspeicherpfad='$XDG_PICTURES_DIR/'
videospeicherpfad='$XDG_VIDEOS_DIR/'
standbildformat='png'
vollbild='false'
vordergrund='true'
" > "$einstellungsdatei"
                fi
            fi
        fi
    fi
    if ! [ -e "$stationsliste" ]; then
        stationsliste_land="$bibliothekspfad/Stationslisten/$ui_lang_4"'.lst'
        if [ -e "$stationsliste_land" ]; then
            cp "$stationsliste_land" "$stationsliste"
        else
            stationsliste_basis="$bibliothekspfad/Stationslisten/$ui_lang_2"'.lst'
            if [ -e "$stationsliste_basis" ]; then
                cp "$stationsliste_basis" "$stationsliste"
            else
                stationsliste_manuell="$bibliothekspfad/Stationslisten/$manuelle_auswahl"'.lst'
                if [ -e "$stationsliste_manuell" ]; then
                    cp "$stationsliste_manuell" "$stationsliste"
                else
                    cat <<< "ARD='https://www.ardmediathek.de/live/Y3JpZDovL2Rhc2Vyc3RlLmRlL2xpdmUvY2xpcC9hYmNhMDdhMy0zNDc2LTQ4NTEtYjE2Mi1mZGU4ZjY0NmQ0YzQ'
ZDF='https://www.zdf.de/sender/zdf/zdf-live-beitrag-100.html'
3sat='https://www.3sat.de/programm/3sat-livestream-100.html'
Arte='https://www.ardmediathek.de/live/Y3JpZDovL2FydGUuZGUvTGl2ZXN0cmVhbS1BUlRF/'
Alpha='https://www.ardmediathek.de/live/Y3JpZDovL2JyLmRlL0xpdmVzdHJlYW0tQVJELUFscGhh/'
Phoenix='https://zdf-hls-19.akamaized.net/hls/live/2016502/de/high/master.m3u8'
Neo='https://www.zdf.de/sender/zdfneo/zdfneo-live-beitrag-100.html'
One='https://www.ardmediathek.de/live/Y3JpZDovL3dkci5kZS9CZWl0cmFnLTFlNjA0YWFlLTViODctNGMzNC04ZDhmLTg4OWI1ZjE2ZDU3Mw/'
KiKa='https://www.ardmediathek.de/live/Y3JpZDovL2tpa2EuZGUvTGl2ZXN0cmVhbS1LaUth/'
Tagesschau24='https://www.ardmediathek.de/live/Y3JpZDovL2Rhc2Vyc3RlLmRlL3RhZ2Vzc2NoYXUvbGl2ZXN0cmVhbQ/'
ZDFinfo='https://www.zdf.de/dokumentation/zdfinfo-doku/zdfinfo-live-beitrag-100.html'
NDR='https://www.ardmediathek.de/live/Y3JpZDovL25kci5kZS9MaXZlc3RyZWFtLU5EUi1OaWVkZXJzYWNoc2Vu/'
MDR='https://www.ardmediathek.de/live/Y3JpZDovL21kci5kZS9MaXZlc3RyZWFtLU1EUi1TYWNoc2Vu/'
WDR='https://www.ardmediathek.de/live/Y3JpZDovL3dkci5kZS9CZWl0cmFnLTNkYTY2NGRlLTE4YzItNDY1MC1hNGZmLTRmNjQxNDcyMDcyYg'
SR='https://www.ardmediathek.de/live/Y3JpZDovL3NyLW9ubGluZS5kZS8yODQ4NjAvbGl2ZXN0cmVhbQ/'
SWF(RP)='https://www.ardmediathek.de/live/Y3JpZDovL3N3ci5kZS8xMzQ4MTIzMA/'
SWF(BW)='https://www.ardmediathek.de/live/Y3JpZDovL3N3ci5kZS8xMzQ4MTA0Mg/'
HR='https://www.ardmediathek.de/live/Y3JpZDovL2hyLmRlL0xpdmVzdHJlYW0tSFI/'
BR='https://www.ardmediathek.de/ard/live/Y3JpZDovL2JyLmRlL0xpdmVzdHJlYW0tQlItTm9yZA'
RB='https://www.butenunbinnen.de/livestream/'
RBB='https://www.ardmediathek.de/ard/live/Y3JpZDovL3JiYi1vbmxpbmUuZGUvcmJiZmVybnNlaGVuL2xpdmVfYnJhbmRlbmJ1cmcvc2VuZGVwbGF0ei0tLWxpdmVzdHJlYW0tLS1icmFuZGVuYnVyZy0tLWhsczE'
DW='https://www.ardmediathek.de/live/Y3JpZDovL2RldXRzY2hld2VsbGUuZGUvTGl2ZXN0cmVhbS1EZXV0c2NoZVdlbGxl'
#Kabel1-Doku='https://www.2ix2.com/kabel-1-doku/'
N24-Doku='https://www.welt.de/tv-programm-n24-doku/'
Welt='https://www.welt.de/tv-programm-live-stream/'
#N-TV='https://www.2ix2.com/n-tv-live/'
#RTL='https://www.2ix2.com/rtl-live/'
#SAT1='https://www.2ix2.com/sat1/'
#PRO7='https://www.2ix2.com/pro7/'
#Kabel1='https://www.2ix2.com/kabel-1/'
#RTL2='https://www.2ix2.com/rtl2-live/'
#VOX='https://www.2ix2.com/vox/'
#Tele5='https://www.2ix2.com/tele-5/'
#SuperRTL='https://www.2ix2.com/super-rtl-live/'
#7Max='https://www.2ix2.com/prosieben-maxx/'
#Nitro='https://www.2ix2.com/rtl-nitro-hd/'
#Gold='https://www.2ix2.com/sat1-gold/'
#DMax='https://www.2ix2.com/dmax/'
#Sixx='https://www.2ix2.com/sixx/'
DW-de='https://www.youtube.com/watch?v=hQ_idJqebjk'
#ORF-1='https://www.2ix2.com/orf1/'
#ORF-2='https://www.2ix2.com/orf2/'
#ORF-3='https://www.2ix2.com/orf3/'
#ServusTV='https://www.2ix2.com/servus-tv/'
#SRF-1='https://www.2ix2.com/srf-1/'
#SRF-2='https://www.2ix2.com/srf-2/'
#SRF-3='https://www.2ix2.com/3-plus/'
#SRF-4='https://www.2ix2.com/4-plus/'
#SRF-5='https://www.2ix2.com/5-plus/'
#SRF-6='https://www.2ix2.com/6-plus/'
#SRF-I='https://www.2ix2.com/srf-info/'
#MTV='https://www.2ix2.com/mtv/'
#Disney='https://www.2ix2.com/disney-channel/'
#Commedy='https://www.2ix2.com/comedy-central/'
#SkySport='https://www.2ix2.com/sky-sport-news-hd-snhd/'
#Sport1='https://www.2ix2.com/sport1/'
" > "$stationsliste"
                fi
            fi
        fi
    fi
}

function programmhilfe {
# Programmhilfe bereitstellen (supply program help)
# Behelfskonstruktion als Ersatz für in gtkdialog nicht funktionierende exportierte Funktionen (Workaround for...)
echo '#!/bin/bash
/bin/bash "'$temporaerdatei_03'" &
while true; do    # we have to wait for settings dialog being displayed again, otherwise it will overlay the manual window
    wid_einstellungen="$(xdotool search --name "'$"aCSTV Einstellungen verwalten"'")"
    if [ "$wid_einstellungen" != "" ]; then break; fi
    sleep .5
done
wid_anleitung="$(xdotool search --name $(rev <<<"'$anleitung'" | cut -d/ -f1 | rev))"
if [ -z $wid_anleitung ]; then
    if which mupdf >/dev/null; then coproc mupdf "'$anleitung'"; else coproc xdg-open "'$anleitung'"; fi
else
    xdotool windowmap $wid_anleitung
    xdotool windowraise $wid_anleitung
fi
' > "$temporaerdatei_09"
chmod 755 "$temporaerdatei_09"
}
programmhilfe

function einstellungen_lesen {
# Einlesen der Einstellungsdatei (read config file)
# (Wegen der nichtfunktionalen Exportfunktion zu gtkdialog müssen diese Variablen ggf. in den Temporärskripten der Behelfskonstruktionen erneut erzeugt werden.) (Workaround for...)
bitrate=$(sed -n "/^kbps=/ s/kbps='\([0-9][0-9]*\)'/\1/p" "$einstellungsdatei")
bildschirm=$(sed -n "/^bildschirm=/ s/bildschirm='\([0-9][0-9]*\)'/\1/p" "$einstellungsdatei")
export informationen=$(sed -n "/^programminformationen=/ s/programminformationen='\(..*\)'/\1/p" "$einstellungsdatei")
export speicherpfad_standbild=$(sed -n "/^standbildspeicherpfad=/ s/standbildspeicherpfad='\(..*\)'/\1/p" "$einstellungsdatei")
export format_standbild=$(sed -n "/^standbildformat=/ s/standbildformat='\(..*\)'/\1/p" "$einstellungsdatei")
export speicherpfad_video=$(sed -n "/^videospeicherpfad=/ s/videospeicherpfad='\(..*\)'/\1/p" "$einstellungsdatei")
export einstellungen="--screen=0 $fs --fs-screen=$bildschirm --ytdl-format='best[tbr<=$bitrate]/bestvideo[tbr<=$bitrate]+bestaudio'"
vorschlag_bildschirm="all|current|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32"
export vorschlag_bildschirm="$(sed "s/"$bildschirm"/\^"$bildschirm"/" <<< $vorschlag_bildschirm)"
vorschlag_bitrate="128|348|400|750|1000|1024|1200|1500|1750|1999|2250|2500|2750|3000|3500|4000|4500|5000|6000|7000|10000|20000|40000"
export vorschlag_bitrate="$(sed "s/^/"$bitrate"|/; s/|/\n/g" <<< "$vorschlag_bitrate" | sort -un | tr '\n' '|' | sed "s/.$//;s/"$bitrate"/\^"$bitrate"/")"
export vorschlag_programminfo="$informationen"
vollbild=$(sed -n "/^vollbild=/ s/vollbild='\(..*\)'/\1/p" "$einstellungsdatei")
vordergrund=$(sed -n "/^vordergrund=/ s/vordergrund='\(..*\)'/\1/p" "$einstellungsdatei")
if $vollbild; then fs="-fs"; else fs=""; fi
if $vordergrund; then ot="--ontop"; else ot=""; fi
}
export einstellungen_lesen

function mpv_starten {
# starte mpv im idle modus mit ipv-server zur weiteren Steuerung. (start mpv in idle mode configured for using IPV server)
nice -n -5 mpv --config-dir="$konfigurationspfad/mpv" --no-ytdl --geometry=25:25 --autofit=50%x50% $MpvRahmen --input-ipc-server="$mpvsocket" --idle=yes --screen=0 $fs --fs-screen=$bildschirm $ot --ytdl-raw-options=no-video-multistreams=,audio-multistreams=,abort-on-error= --ytdl-format='best[tbr<='$bitrate']/bestvideo[tbr<='$bitrate']+mergeall' --hls-bitrate=$(($bitrate*10**3)) & jobs -p > $temporaerdatei_02
touch "$temporaerdatei_08"
}

function aufnahme {
# Aufnahmefunktion (recording function)
# Behelfskonstruktion für nichtfunktionalen IPV-Befehl für die Aufnahme in mpv. (Workaround for nonfunctional stream-record command in mpv IPV interface)
echo -e "#!/bin/bash
bitrate=\"\$(sed -n \"/^kbps=/ s/kbps='\([0-9][0-9]*\)'/\1/p\" \""$einstellungsdatei"\")\"
bildschirm=\"\$(sed -n \"/^bildschirm=/ s/bildschirm='\([0-9][0-9]*\)'/\1/p\" \""$einstellungsdatei"\")\"
speicherpfad_video=\"\$(sed -n \"/^videospeicherpfad=/ s/videospeicherpfad='\(..*\)'/\1/p\" \""$einstellungsdatei"\")\"
export videodatei=\""$"Sendungsmitschnitt""-\$(cat \""$temporaerdatei_08"\")-\$(date +%d.%b.%Y-%H:%M:%S).ts\"
nice -n -5 mpv --config-dir="$konfigurationspfad/mpv" --no-ytdl --geometry=25:25 --autofit=50%x50% $MpvRahmen --input-ipc-server=\""$mpvsocket"\" --idle=yes --stream-record=\"\$speicherpfad_video\$videodatei\" --screen=0 $fs --fs-screen=\$bildschirm $ot --ytdl-raw-options=no-video-multistreams=,audio-multistreams=,abort-on-error= --ytdl-format='best[tbr<='\$bitrate']/bestvideo[tbr<='\$bitrate']+mergeall' --hls-bitrate=\$((\$bitrate*10**3)) \$(cat $temporaerdatei_04) & jobs -p > \""$temporaerdatei_02"\"
touch \""$temporaerdatei_05"\"
rm -f \""$temporaerdatei_11"\"
while ! echo \"{ \\\"command\\\": [\\\"get_property\\\", \\\"idle-active\\\"] }\" | socat - \""$mpvsocket"\" | grep \x22success\x22; do sleep .5; done
echo \x27{ \x22command\x22: [\x22set_property\x22, \x22screenshot-template\x22, \x22"$"Szenenfoto""-\x27\$(cat $temporaerdatei_08)\x27-%td.%tm.%tY-%tH:%tM:%tS\x22] }\x27 | socat - \""$mpvsocket"\"
echo \x27{ \x22command\x22: [\x22set_property\x22, \x22screenshot-directory\x22, \x22\x27\$(sed -n \x22/^standbildspeicherpfad=/ s/standbildspeicherpfad=\\x27\\(..*\\)\\x27/\\1/p\x22 \x22$einstellungsdatei\x22)\x27\x22] }\x27 | socat - \""$mpvsocket"\"
echo \x27{ \x22command\x22: [\x22set_property\x22, \x22screenshot-format\x22, \x22\x27\$(sed -n \x22/^standbildformat=/ s/standbildformat=\\x27\\(..*\\)\\x27/\\1/p\x22 \x22$einstellungsdatei\x22)\x27\x22] }\x27 | socat - \""$mpvsocket"\"" > "$temporaerdatei_06"
cat <<<'
while [ -e "'$temporaerdatei_05'" ]; do if [ $(($(bc <<<"$(($(stat -f --format="%a*%S" "$Speicherpfad_video")))/$(bc <<<"$(($(stat --format="%b*%B" "$speicherpfad_video$videodatei")))/($(date +%s) - $(stat -c "%X" "$speicherpfad_video$videodatei"))")")/60)) -le 10 ]; then yad --image=aCSTV2 --window-icon=aCSTV2 --borders=10 --center --fixed '$YadRahmen' --title="'$"aCSTV Aufnahme"'" --text="\t""'$"Achtung! Es ist nur noch Speicherplatz \n\tfür 10 Minuten Aufnahmezeit verfügbar."'" --button=OK;break;fi;sleep 300;done &
exec 9<> "$fifo_warteschlange_03"
coproc (yad --borders=10 $YadRahmen --sticky --fixed --width=200 height=150 \
    --window-icon="$symbol_aufnahme" \
    --title="'$"aCSTV Aufnahme"'" \
    --text="'$"Laufende aCSTV Aufzeichnung"'" \
    --command='"'"'bash "'$temporaerdatei_19'" "'"'"'"$speicherpfad_video$videodatei"'"'"'"'"'"' \
    --no-middle --listen \
    --notification <&9)
echo "icon:"'$symbol_aufnahme'"" 2>&1 > "'$fifo_warteschlange_03'"
echo "tooltip:"'$"Laufende aCSTV Aufzeichnung"'"" 2>&1 > "'$fifo_warteschlange_03'"
exec 9>&-
' >> "$temporaerdatei_06"
chmod 755 "$temporaerdatei_06"
}

function aufnahmestop {
# Funktion zur Rückkehr zur Wiedergabe nach Aufnahmeende. (Function for returning to normal playback after recording)
# Behelfskonstruktion für nichtfunktionalen IPV-Befehl für das Stoppen der Aufnahme in mpv. (Workaround for nonfunctional IPV-command for stopping a record)
echo -e "#!/bin/bash
bitrate=\"\$(sed -n \"/^kbps=/ s/kbps='\([0-9][0-9]*\)'/\1/p\" \""$einstellungsdatei"\")\"
bildschirm=\"\$(sed -n \"/^bildschirm=/ s/bildschirm='\([0-9][0-9]*\)'/\1/p\" \""$einstellungsdatei"\")\"
nice -n -5 mpv --config-dir="$konfigurationspfad/mpv" --no-ytdl --geometry=25:25 --autofit=50%x50% $MpvRahmen --input-ipc-server=\""$mpvsocket"\" --idle=yes --screen=0 $fs --fs-screen=\$bildschirm $ot --ytdl-raw-options=no-video-multistreams=,audio-multistreams=,abort-on-error= --ytdl-format='best[tbr<='\$bitrate']/bestvideo[tbr<='\$bitrate']+mergeall' --hls-bitrate=\$((\$bitrate*10**3)) & jobs -p > \""$temporaerdatei_02"\"
exec 9<> "$fifo_warteschlange_03"
wmctrl -c \""$"aCSTV Aufnahme""\"
echo \"quit\" 2>&1 > "'$fifo_warteschlange_03'"
exec 9>&-
" > "$temporaerdatei_07"
chmod 755 "$temporaerdatei_07"
}

function einstellungen_ändern {
# Funktion zur Änderung der Grundeinstellungen in der Konfigurationsdatei (Function for modifying sttings file)
# Behelfskonstruktion als Ersatz für in gtkdialog nicht funktionierende exportierte Funktionen: (Workaround for...)
# Funktion in temporaerdatei schreiben anstatt sie zu exportieren. (Write functions into files instead of exporting them)
cat <<<'#!/bin/bash
acstv_wid=""
while true; do
    acstv_wid=$(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
    if [ "$acstv_wid" != "" ]; then
        sleep .2
        xdotool windowunmap $acstv_wid
        break  
    fi
    sleep .2
done &
bitrate=$(sed -n "/^kbps=/ s/kbps=\x27\([0-9][0-9]*\)\x27/\1/p" "$einstellungsdatei")
bildschirm=$(sed -n "/^bildschirm=/ s/bildschirm=\x27\([0-9][0-9]*\)\x27/\1/p" "$einstellungsdatei")
informationen=$(sed -n "/^programminformationen=/ s/programminformationen=\x27\(..*\)\x27/\1/p" "$einstellungsdatei")
speicherpfad_standbild=$(sed -n "/^standbildspeicherpfad=/ s/standbildspeicherpfad=\x27\(..*\)\x27/\1/p" "$einstellungsdatei")
format_standbild=$(sed -n "/^standbildformat=/ s/standbildformat=\x27\(..*\)\x27/\1/p" "$einstellungsdatei")
speicherpfad_video=$(sed -n "/^videospeicherpfad=/ s/videospeicherpfad=\x27\(..*\)\x27/\1/p" "$einstellungsdatei")
vorschlag_bildschirm="all|current|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32"
vorschlag_bildschirm="$(sed "s/"$bildschirm"/\^"$bildschirm"/" <<< $vorschlag_bildschirm)"
vorschlag_bitrate="128|348|400|750|1000|1024|1200|1500|1750|1999|2250|2500|2750|3000|3500|4000|4500|5000|6000|7000|10000|20000|40000"
vorschlag_bitrate="$(sed "s/^/"$bitrate"|/; s/|/\n/g" <<< "$vorschlag_bitrate" | sort -un | tr "\n" "|" | sed "s/.$//;s/"$bitrate"/\^"$bitrate"/")"
vorschlag_programminfo="$informationen"
vorschlag_standbildformat="jpg|png|webp"
vorschlag_standbildformat="$(sed "s/"$format_standbild"/\^"$format_standbild"/" <<< $vorschlag_standbildformat)"
vollbild=$(sed -n "/^vollbild=/ s/vollbild=\x27\(..*\)\x27/\1/p" "$einstellungsdatei")
vordergrund=$(sed -n "/^vordergrund=/ s/vordergrund=\x27\(..*\)\x27/\1/p" "$einstellungsdatei")

offenhalten=true
while $offenhalten; do

ausgabe_einstellungen="$(yad $YadRahmen --fixed --height=200 --width=200 --center --borders=10 \
        --window-icon="'$symbol_einstellungen'" \
        --title="'$"aCSTV Einstellungen verwalten"'" \
        --text="'"<b>"$"aCSTV Einstellungen""</b>\n\n"$"Bitte geben sie die gewünschten Voreinstellungen ein bzw. wählen""\n"$"aus den Menüs die entsprechenden Optionen aus.""\n"'" \
        --form \
        --separator="|" \
        --item-separator="|" \
        --columns=2 \
        --field="'$"Max. Videobitrate (in kbps):"'":CE "$vorschlag_bitrate" \
        --field="'$"Ausgabegerät (Bildschirm):"'":CB "$vorschlag_bildschirm" \
        --field="'$"URL für Programminformationen:"'":CE "^$vorschlag_programminfo" \
        --field="'$"Starte Wiedergabe mit Vollbild"'":CHK "$vollbild" \
        --field="'$"Verzeichnis für Szenenfotos:"'":DIR "$speicherpfad_standbild" \
        --field="'$"Ausgabeformat für Szenenfotos:"'":CB "$vorschlag_standbildformat" \
        --field="'$"Verzeichnis für Sendungsmitschnitte:"'":DIR "$speicherpfad_video" \
        --field="'$"Wiedergabe im Vordergrund"'":CHK "$vordergrund" \
        --button="'$"Aktualisieren"'":2  --button="'$"Programmhilfe"'":6 --button="'$"Senderliste bearbeiten"'":8 --button="'$"Abbruch"'":5 --button="'$"Speichern"'":4)"
tastenauswahl=$?

bitrate="$(cut -d "|" -f1 <<< "$ausgabe_einstellungen")"
informationen="$(cut -d "|" -f3 <<< "$ausgabe_einstellungen")"
bildschirm="$(cut -d "|" -f2 <<< "$ausgabe_einstellungen")"
speicherpfad_standbild="$(cut -d "|" -f5 <<< "$ausgabe_einstellungen")"
speicherpfad_video="$(cut -d "|" -f7 <<< "$ausgabe_einstellungen")/"
format_standbild="$(cut -d "|" -f6 <<<"$ausgabe_einstellungen")"
vollbild="$(cut -d "|" -f4 <<< "$ausgabe_einstellungen")"
vollbild="${vollbild,,}"
vordergrund="$(cut -d "|" -f8 <<< "$ausgabe_einstellungen")"
vordergrund="${vordergrund,,}"

acstv_wid=$(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")  # $acstv_wid generated in coproc not present here, so we have to repeat

if [ $tastenauswahl == 4 ]; then         # Speichere Einstellungen
    offenhalten=false
    echo "Speichere Einstellungen in Datei"
    sed -i "s/^kbps=..*/kbps=\x27"$bitrate"\x27/" "$einstellungsdatei"
    sed -i "s/^bildschirm=..*/bildschirm=\x27"$bildschirm"\x27/" "$einstellungsdatei"
    sed -i "s|^programminformationen=..*|programminformationen=\x27$informationen\x27|" "$einstellungsdatei"
    sed -i "s|^standbildspeicherpfad=..*|standbildspeicherpfad=\x27$speicherpfad_standbild\x27|" "$einstellungsdatei"
    sed -i "s|^videospeicherpfad=..*|videospeicherpfad=\x27$speicherpfad_video\x27|" "$einstellungsdatei"
    sed -i "s/^standbildformat=..*/standbildformat=\x27"$format_standbild"\x27/" "$einstellungsdatei"
    sed -i "s/^vollbild=..*/vollbild=\x27"$vollbild"\x27/" "$einstellungsdatei"
    sed -i "s/^vordergrund=..*/vordergrund=\x27"$vordergrund"\x27/" "$einstellungsdatei"
    echo "{ \"command\": [\"set_property\", \"ytdl-format\", \"best[tbr<="$bitrate"]/bestvideo[tbr<="$bitrate"]+bestaudio\"] }" | socat - "'$mpvsocket'"
    echo "{ \"command\": [\"set_property\", \"screenshot-directory\", \""$speicherpfad_standbild"\"] }" | socat - "'$mpvsocket'"
    echo "{ \"command\": [\"set_property\", \"screenshot-format\", \""$format_standbild"\"] }" | socat - "'$mpvsocket'"
    echo "{ \"command\": [\"set_property\", \"screen\", \"0\"] }" | socat - "'$mpvsocket'"
    echo "{ \"command\": [\"set_property\", \"fs-screen\", \""$bildschirm"\"] }" | socat - "'$mpvsocket'"
    if $vollbild; then 
        export fs="-fs"
        echo "{ \"command\": [\"set_property\", \"fs\", \"yes\"] }" | socat - "'$mpvsocket'"
    else
        export fs=""
        echo "{ \"command\": [\"set_property\", \"fs\", \"no\"] }" | socat - "'$mpvsocket'"
    fi
    if $vordergrund; then
        ot=""--ontop""
    else
        ot=""
    fi
    xdotool windowmap $acstv_wid
    wmctrl -i -r $acstv_wid -b add,above
elif [ $tastenauswahl == 0 ]; then
    offenhalten=true
    vorschlag_bildschirm="all|current|0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32"
    vorschlag_bildschirm="$(sed "s/"$bildschirm"/\^"$bildschirm"/" <<< $vorschlag_bildschirm)"
    vorschlag_bitrate="128|348|400|750|1000|1024|1200|1500|1750|1999|2250|2500|2750|3000|3500|4000|4500|5000|6000|7000|10000|20000|40000"
    vorschlag_bitrate="$(sed "s/^/"$bitrate"|/; s/|/\n/g" <<< "$vorschlag_bitrate" | sort -un | tr "\n" "|" | sed "s/.$//;s/"$bitrate"/\^"$bitrate"/")"
    vorschlag_programminfo="$informationen"
    vorschlag_standbildformat="jpg|png|webp"
    vorschlag_standbildformat="$(sed "s/"$format_standbild"/\^"$format_standbild"/" <<< $vorschlag_standbildformat)"
elif [ $tastenauswahl == 6 ]; then        # Programmhilfe anzeigen
    offenhalten=false
    /bin/bash "'$temporaerdatei_09'" &
elif [ $tastenauswahl == 8 ]; then        # Senderliste bearbeiten
    if ! [ -w "$stationsliste" ]; then 
        yad --window-icon="'$symbol_fehler'" --timeout=3 --title="'$"Fehler"'" \
            --text="<b>""'$"Keine Schreibrechte für die Stationslistendatei."'""</b>\n\t""'$"Bitte kopieren Sie die gewünschte Stationsliste\n\taus der für alle Nutzer verfügbaren Bibliothek in\n\tIhr Home-Verzeichnis um sie bearbeiten oder\n\taktualisieren zu können, und benennen Sie sie\n\tIhren Wünschen entsprechend."'" \
            --no-buttons $YadRahmen --center --borders=15 --on-top --fixed
        offenhalten=true
    else
        offenhalten=false
        /bin/bash "'$temporaerdatei_12'"
    fi
elif [ $tastenauswahl == 2 ]; then        # Senderliste automatisch aktualisieren
    if ! [ -w "$stationsliste" ]; then 
        yad --window-icon="'$symbol_fehler'" --timeout=3 --title="'$"Fehler"'" \
            --text="<b>""'$"Keine Schreibrechte für die Stationslistendatei."'""</b>\n\t""'$"Bitte kopieren Sie die gewünschte Stationsliste\n\taus der für alle Nutzer verfügbaren Bibliothek in\n\tIhr Home-Verzeichnis um sie bearbeiten oder\n\taktualisieren zu können, und benennen Sie sie\n\tIhren Wünschen entsprechend."'" \
            --no-buttons $YadRahmen --center --borders=15 --on-top --fixed    
        offenhalten=true
    else
        offenhalten=false
        while $flag_retry; do
            if nc -zw1 $ip_fuer_verbindungspruefung; then
                flag_retry=false
                /bin/bash "'$temporaerdatei_15'"
            else
                yad --center --fixed $YadRahmen --borders=10 \
--window-icon="$symbol_internet" \
--title=$"Verbindungsfehler" \
--text="<b>"$"Keine Internetverbindung gefunden.""</b>\n\n"$"Sie können es erneut versuchen oder die Aktualisierung beenden.""\n" \
--button=$"Abbrechen":4 --button=$"Erneut versuchen":6
                tastenauswahl2=$?
                if [ $tastenauswahl2 == 4 ]; then
                    exit 1
                elif [ $tastenauswahl2 == 6 ]; then
                    flag_retry=true
                fi
            fi
        done
    fi
elif [ $tastenauswahl == 5 ] || [ $tastenauswahl == 252 ]; then        # Abbruch, Hauptfenster wiederherstellen
    offenhalten=false
    xdotool windowmap $acstv_wid
    wmctrl -i -r $acstv_wid -b add,above
fi
done
' > "$temporaerdatei_03"
chmod 755 "$temporaerdatei_03"
}

function übergroße_stationsliste {
# Nutzerinformation bei überlanger Wartezeit durch zu große Stationsliste (display user information when oversized station list renders aCSTV processing slow.)
e=$(grep -c -v "^#" "$stationsliste")
if [ $e -gt 60 ]; then
    yad $YadRahmen --borders=15 --fixed --width=450 --height=150 --posx="50" --posy="50" --skip-taskbar \
    --text="<b>"$"Exzessive Senderliste.""</b> ""($e"" Einträge"")\n\n"$"Die Verarbeitung von Senderlisten mit mehr als 50-60 aktiven\nEinträgen kann die Reaktionszeit von aCSTV erheblich verlangsamen.\n"$"Bitte erwägen Sie, Ihre Senderliste zugunsten einer beschleunigten\nReaktion in verschiedenen aCSTV Funktionen zu beschränken. "$"Dies\nbetrifft u.a. den Programmstart und die Bearbeitung von Senderlisten.""\n\n<b>"$"Bitte gedulden Sie sich, bis die Verarbeitung abgeschlossen ist.""</b>" --window-icon="$symbol_aCSTV" \
    --no-buttons --timeout="15" --timeout-indicator="left" --close-on-unfocus &
fi
}

function stationsliste_einlesen {
# Stationsliste einlesen (read stations list file)
übergroße_stationsliste
sender=()
adresse=()
c_len=()
len=0
o=$(($1*60))
i=0
while read -e b; do
    [ "${b::1}" == "#" ] || [ "$b" == "" ] && continue
    a=("$(cut -d= -f1 <<<"$b")")
    if [ $(wc -c <<<"$a") -ge 13 ]; then
       a=${a::12}
       k=1
       if [ $(grep -c "$a" <<<"${sender[@]}") -gt 0 ]; then
           while [ $(grep -c "$a$k" <<<"${sender[@]}") -gt 0 ]; do
               let $((k++))
           done
           a="$a$k"
       fi
    fi
    sender+=("$a")
    adresse+=("$(cut -d= -f2- <<<"$b")")
    c_len+=($(($(cut -d= -f1 <<<"$a" | tr -d "\n" | wc -m)+4)))  # zusätzliche 4 Zeichen für die Berechnungen pro Taste als Äquivalent für die Tastenabstände (add 4 more characters per button as an equivalent for button space in line)
    len=$(($len+${c_len[$i]}))  # Gesamtzeichenzahl im Tastenfeld
    let $((i++))
done <<<"$(IFS= head -n$o "$stationsliste" | tail -n60)"
[ $i -eq 0 ] && return 1  || return 0
}
export stationsliste_einlesen

function stationsliste_bearbeiten {
# Einträge in Stationsliste ändern, hinzufügen oder löschen (edit stations list file)
# Behelfskonstruktion als Ersatz für in gtkdialog nicht funktionierende exportierte Funktionen: (Workaround for...)
# Funktion in temporaerdatei schreiben anstatt sie zu exportieren. (Write function to tempfile instead of exporting it)
cat <<<'#!/bin/bash
# Nutzerinformation (user information, duplicate due to exported function not available here)
     function übergroße_stationsliste { 
         e=$(grep -c -v "^#" "$stationsliste")
         if [ $e -gt 60 ]; then
             yad $YadRahmen --borders=15 --fixed --width=450 --height=150 --posx="50" --posy="50" --skip-taskbar \
                 --text="<b>"$"Exzessive Senderliste.""</b> ""($e"$" Einträge"")\n\n"$"Die Verarbeitung von Senderlisten mit mehr als 50-60 aktiven\nEinträgen kann die Reaktionszeit von aCSTV erheblich verlangsamen.\n"$"Bitte erwägen Sie, Ihre Senderliste zugunsten einer beschleunigten\nReaktion in verschiedenen aCSTV Funktionen zu beschränken. "$"Dies\nbetrifft u.a. den Programmstart und die Bearbeitung von Senderlisten.""\n\n<b>"$"Bitte gedulden Sie sich, bis die Verarbeitung abgeschlossen ist.""</b>" --window-icon="$symbol_aCSTV" \
                 --no-buttons --timeout="15" --timeout-indicator="left" --close-on-unfocus &
         fi
     }
übergroße_stationsliste
# fifo-Warteschlange initialisieren (initialise fifo queue)
    export fifo_warteschlange_01="/dev/shm/aCSTV-fifo-'"$(echo $$)"'01.tmp"
    mkfifo "$fifo_warteschlange_01"
    exec 3<> "$fifo_warteschlange_01"

    # Arbeitskopie der Senderliste anfertigen  (create working copy of stations list file)
    cp "$stationsliste" "'"$temporaerdatei_14"'"

    function warteschlange_fuellen {
    # Warteschlange für die Bearbeitung der Stationsliste aus der Arbeitskopie befüllen  (fill up fifo queue with entries from working copy of stations list file)
        OrigIFS=$IFS
        IFS="$(printf '\''|\n\t'\'')"
        i=0
>"$temporaerdatei_17"  # additional temp file added to take huge stations lists for additional fifo buffer
        for eintrag in $(cat "'"$temporaerdatei_14"'"); do 
            #if [ "${eintrag:0:1}" != "#" ]; then        # Bedingung entfernt um die Bearbeitung deaktivierter Einträge zu ermöglichen (condition removed to allow user to see and edit deactivated entries)
                echo $((++i)) >> "$temporaerdatei_17"
                echo false >> "$temporaerdatei_17"
                echo "$eintrag" | cut -d= -f1 >> "$temporaerdatei_17"
                echo "$eintrag" | cut -d= -f2- | sed -e "s/^\x27//; s/\x27\$//">> "$temporaerdatei_17"
            #fi
        done
        cat "$temporaerdatei_17" | buffer -m 256k > "$fifo_warteschlange_01" &   # (direct fifo replaced by additional temp file and buffered fifo due to huge US-American stations lists, containing more than 1000 entries, exceeding default fifo buffer size)
        echo $i > "'"$temporaerdatei_13"'"
        IFS=$OrigIFS
    }

    function eintrag_hinzufuegen {
    # Neuen Eintrag in die Sationsliste aufnehmen  (add new entry to stations list)
        let $(( i=$(cat "'"$temporaerdatei_13"'")  + 1 )); echo $i > "'"$temporaerdatei_13"'"
        echo $i
        echo false
        echo ""
        echo ""
    }
    export -f eintrag_hinzufuegen

    function eintraege_loeschen {
    # Ausgewählte Einträge aus der Stationsliste löschen
> "'"$temporaerdatei_14"'"
        while read -r eintrag; do
            if ! $(cut -d'\''|'\'' -f2 <<< "${eintrag,,}"); then
                echo -e "$(cut -d'\''|'\'' -f3 <<< "$eintrag")=\x27$(cut -d'\''|'\'' -f4 <<< "$eintrag")\x27" >> "'"$temporaerdatei_14"'"
            fi
        done <<< "$senderliste_neu"
    }
    export -f eintraege_loeschen

    function aenderungen_verwerfen {
    # Keine Änderungen an der Stationsliste vornehmen  (don’t apply any changes to stations list)
        rm -f "'"$temporaerdatei_14"'"
        rm -f "'"$temporaerdatei_13"'"
        rm -f "$fifo_warteschlange_01"
    }

    function aenderungen_speichern {
    # Geänderte Stationsliste speichern  (save changes to stations list)
    > "'"$temporaerdatei_14"'"
        while read -r eintrag; do
            echo -e "$(cut -d'\''|'\'' -f3 <<< "$eintrag")=\x27$(cut -d'\''|'\'' -f4 <<< "$eintrag")\x27" >> "'"$temporaerdatei_14"'"
        done <<< "$senderliste_neu"
        mv "'"$stationsliste"'" "'"$stationsliste"'.bak"
        cp "'"$temporaerdatei_14"'" "'"$stationsliste"'"
        rm -f "'"$temporaerdatei_14"'"
        rm -f "'"$temporaerdatei_13"'"
        rm -f "$fifo_warteschlange_01"
    }

    function sender_bearbeiten {
    # Dialog Senderliste bearbeiten   (stations list edit dialog)
        senderliste_neu=$(yad --width=700 --height=500 --fixed --center $YadRahmen --borders=10 \
--window-icon="'$symbol_einstellungen'" \
--title="'$"aCSTV Senderliste verwalten"'" \
--text="<b>""'$"aCSTV Senderliste"'""</b>\n""'$"Alle Einträge lassen sich per Doppelklick bearbeiten."'""\n""'$"Sollen Einträge entfernt werden, diese mit einem Häkchen markieren und die Taste <i>»Einträge Entfernen«</i> drücken."'""\n""'$"Neue Einträge mit der Taste <i>»Neuen Eintrag hinzufügen«</i> anlegen und anschließend bearbeiten."'" \
--list --print-all --no-click \
--listen \
--separator="|" --grid-lines=hor \
--editable --editable-cols="3,4" \
--column "'$"Nr."'":NUM --column "☮":CHK --column "'$"Sender"'":TEXT --column "'$"Adresse (URL)"'":TEXT \
--search-column=3 \
--button="'$"Markierte Einträge löschen"'":4 \
--button="'$"Neuen Eintrag hinzufügen"'":'\''bash -c "eintrag_hinzufuegen > $fifo_warteschlange_01"'\'' \
--button="'$"Änderungen verwerfen"'":6 \
--button="'$"Senderliste speichern"'":8 < $fifo_warteschlange_01)
    }

    while :; do
        warteschlange_fuellen
        sender_bearbeiten
        tastenauswahl="$?"
        if [ $tastenauswahl == 4 ]; then              # markierte Eeinträge löschen
            eintraege_loeschen
        elif [ $tastenauswahl == 6 ] || [ $tastenauswahl == 252 ]; then            # Änderungen nicht speichern
            aenderungen_verwerfen
            /bin/bash "'$temporaerdatei_03'" &
            exit 6
            break
        elif [ $tastenauswahl == 8 ]; then            # Änderungen speichern
            übergroße_stationsliste
            aenderungen_speichern
            while [ -f "'"$temporaerdatei_14"'" ]; do sleep .5; done
            xdotool windowclose $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
            kill -15 $(cut -d- -f2 <<< "'"$temporaerdatei_01"'" | rev | cut -c 7- | rev)
            setsid /bin/bash aCSTV.sh -s "'"$stationsliste_nutzer"'" & exit 0
        fi
    done
' > "$temporaerdatei_12"
chmod 755 "$temporaerdatei_12"
}
export stationsliste_bearbeiten

function stationsliste_aktualisieren {
# Stationsliste automatisch per Internet aktualisieren (actualise stations list from internet)
    cat <<<'#!/bin/bash
    acstv_wid=$(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
    #xdotool windowunmap $acstv_wid
    
    wget -O "$temporaerdatei_22" "'"$aktualisierung"'"
    if [ $? -ne 0 ]; then
        echo $"Error: list of stations lists not found on server"
        return
    fi
    st_liste=$(cat "$temporaerdatei_22" | tr "," "\n" | tr "[" "\n" |sed -n '"'"'s/{"name":"\(..*\)\.m3u"/\1/p'"'"' | tr "\n" "|")
    st_liste=${st_liste::-1}
    st_liste=$(sed "s/^\(..*\)$/|\1|/;s/|\('$ui_land'\)|/|^\1|/;s/^|\(..*\)|$/\1/" <<<"$st_liste")   #mark user country for yad
    st_check=true

# Nutzerinformation (user information, duplicate due to exported function not available here)
     function übergroße_stationsliste { 
         e=$(grep -c -v "^#" "$stationsliste")
         if [ $e -gt 60 ]; then
             yad $YadRahmen --borders=15 --fixed --width=450 --height=150 --posx="50" --posy="50" --skip-taskbar \
                 --text="<b>"$"Exzessive Senderliste.""</b> ""($e"" Einträge"")\n\n"$"Die Verarbeitung von Senderlisten mit mehr als 50-60 aktiven\nEinträgen kann die Reaktionszeit von aCSTV erheblich verlangsamen.\n"$"Bitte erwägen Sie, Ihre Senderliste zugunsten einer beschleunigten\nReaktion in verschiedenen aCSTV Funktionen zu beschränken. "$"Dies\nbetrifft u.a. den Programmstart und die Bearbeitung von Senderlisten.""\n\n<b>"$"Bitte gedulden Sie sich, bis die Verarbeitung abgeschlossen ist.""</b>" --window-icon="$symbol_aCSTV" \
                 --no-buttons --timeout="15" --timeout-indicator="left" --close-on-unfocus &
         fi
     }

status_info() {
    status_count=1
    mkfifo "$fifo_warteschlange_02"
    exec 3<> $fifo_warteschlange_02
    coproc (yad --progress --auto-close \
    --width=300 \
    --posx=75 --posy=100 --borders=10 \
    $YadRahmen --window-icon="'$symbol_aCSTV'" \
    --title="'$"aCSTV Senderliste aktualisieren"'" – "'$"Sender prüfen"'" \
    --text="\n<b>""'$"aCSTV Senderliste aktualisieren"'""</b>\n\t""'$"Verbindungsaufbau zum Sender wird geprüft:"'""\n" \
    --button="'$"Abbrechen"'":1 <&3
    exitcode=$?
    [ $exitcode -eq 1 ] || [ $exitcode -eq 252 ] && exit 1)
    exec 3>&-
}

    nutzervorgabe=$(yad --center --fixed $YadRahmen --borders=10 \
        --window-icon="'$symbol_einstellungen'" \
        --title="'$"Stationsliste aktualisieren"'" \
        --text="<b>""'$"aCSTV Stationsliste aktualisieren"'""</b>\n\n""'$"Bitte warten Sie, bis die Aktualisierung abgeschlossen ist."'""\n" \
        --form --item-separator="|" \
        --field="'$"Länderlisten:"'":CB "$st_liste" \
        --field="'$"Prüfe auf Verfügbarkeit"'":CHK "$st_check" \
        --button="'$"Abbrechen"'":4 --button="'$"Aktualisierung starten"'":6)
    tastenauswahl2=$?
    if [ $tastenauswahl2 -eq 4 ] || [ $tastenauswahl2 -eq 252 ]; then
    xdotool windowmap $acstv_wid
    wmctrl -i -r $acstv_wid -b add,above
        exit 1
    elif [ $tastenauswahl2 -eq 6 ]; then
        st_land=$(cut -d"|" -f1 <<<"$nutzervorgabe")
        st_check=$(cut -d"|" -f2 <<<"$nutzervorgabe"); st_check=${st_check,,}
        wget -O "$temporaerdatei_16" "https://github.com/iptv-org/iptv/raw/master/streams/$st_land.m3u"
        [ $? -ne 0 ] && exit 1
        sed -i -e "/#EXTM3U\|EXTVLCOPT/d" "$temporaerdatei_16"
        sed -n -e "/^#EXTINF:/{s/..*,\(..*\) *\((\.*\| *\)\(\[.*\| *\)\((.*\| *\).*$/\1/;s/ (..*)//;s/ \[..*\]//;s/ *//g;N;s/\n/=\x27/;s/$/\x27/p}" "$temporaerdatei_16" > "$temporaerdatei_14"
        rm -f "$temporaerdatei_16"
        if $st_check; then status_info; fi
        awk '"'"'!seen[$0]++'"'"' "$temporaerdatei_14" > "$temporaerdatei_16"   #remove duplicate entries
        cp "$temporaerdatei_16" "$temporaerdatei_14"
        anzahl=$(wc -l <<<$(cat "$temporaerdatei_14"))
        faktor=$((99*1000/$anzahl))
        i=0
        while read k; do
            let $((i++))
            [ ${k::1} == "#" ] && continue  # skip comment lines
            m="$(cut -d= -f1 <<<"$k")"      # extract token
            if [ $(grep -c "^$m=" "$temporaerdatei_14") -gt 1 ]; then   # check for duplicate tokens
                r=1
                while [ $(grep -c "^$m$r=" "$temporaerdatei_14") -ge 1 ]; do
                    let $((r++))
                done
                echo change $r
                sed -i "/${k//\//\\/}/s/^\(${m//\//\\/}\)/\1$r/" "$temporaerdatei_14"
            fi                
            if $st_check; then  
                l="$(cut -d= -f2 <<<$k | cut -c2- | rev | cut -c2- | rev)"
                echo \#"'$"Prüfe"'"" $l" 2>&1 >> $fifo_warteschlange_02
                echo "$(($faktor*$i/1000))" 2>&1 >> $fifo_warteschlange_02
                yt-dlp -F "$l"
                [ $? -ne 0 ] && sed -i "s/^\(${k//\//\\/}\)$/#\1/" "$temporaerdatei_14"
            fi
        done <"$temporaerdatei_16"
        rm -f "$temporaerdatei_16"
        if $st_check; then echo "100" 2>&1 >> $fifo_warteschlange_02; fi
        mv "$stationsliste" "$stationsliste.prev"
        mv "$temporaerdatei_14" "$stationsliste"
        übergroße_stationsliste
    fi
    xdotool windowclose $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
    kill -15 $(cut -d- -f2 <<< "'"$temporaerdatei_01"'" | rev | cut -c 7- | rev)
    sleep .5
    setsid /bin/bash aCSTV.sh -s "'"$stationsliste_nutzer"'" & exit 0

#todo: check whether new list is not empty before overwriting previous list.
#todo: allow local m3u file resource
#todo: checkbox to allow keep existing stations

' > "$temporaerdatei_15"
chmod 755 "$temporaerdatei_15"
}
export stationsliste_aktualisieren

function dialogaufteilung {
# Verhältnis von Zeilenanzahl zu Zeilenlänge für die Dialogaufteilung ermitteln (calculate form factor to make a nice look for any number of buttons)
#w=750 # Formfaktor; 0/000; todo: muß sich für große Tastenfelder auf 1200 ändern
w=640
for g in $(seq 22); do    # Anzahl der erforderlichen Zeilen abschätzen (calculate aprox. required line count)
    [ $((($len/$g)/($len/$i)*$w/1000)) -le $(($g)) ] && break
done
if [ $g -ne 1 ]; then
    l=$(($len*$w/($g-1)/1000))   # angestrebte Anzahl von Zeichen pro Zeile abschätzen (calculate aprox. required character count per line)
else
    l=$len
fi
tastenanzahl=${#c_len[*]}  # Stationstastenanzahl bereitstellen (provide station button count)
}
export dialogaufteilung

function verbindungstest {
# Internetverbindungstest (check internet connection)
flag_retry=true
while $flag_retry; do
    if ! nc -zw1 $ip_fuer_verbindungspruefung; then
        yad --center --fixed $YadRahmen --borders=10 \
--window-icon="$symbol_internet" \
--title=$"Verbindungsfehler" \
--text="<b>"$"Keine Internetverbindung gefunden.""</b>\n\n"$"Stellen Sie bitte eine Internetverbindung her, bevor""\n"$"Sie es erneut versuchen, oder Sie können aCSTV beenden.""\n" \
--button=$"aCSTV beenden":4 --button=$"Erneut versuchen":6
        tastenauswahl=$?
        if [ $tastenauswahl == 4 ] || [ $tastenauswahl -eq 252 ]; then
            exit 1
        elif [ $tastenauswahl == 6 ]; then
            flag_retry=true
        fi
    else
        break
    fi
done
}
export -f verbindungstest

# gtkdlg Hauptdialog aCSTV (gtk main dialog routine)
function hauptdialog {
echo '<window title="'$"aCSTV antiX Community Simple TV Starter"'" decorated="'$GtkdlgRahmen'" icon-name="'$symbol_aCSTV'" skip_taskbar_hint="false" resizable="false">
    <vbox>
    <frame '$"TV-Sender"' >' > "$temporaerdatei_01"
if [ $1 -gt 1 ]; then
    echo ' <hbox>
            <button tooltip-text="'$"vorherige Seite"'">
                <variable>taste_seite+</variable>
                <label>"'$" ← "'"</label>
                <action>echo "quit" | socat - "'$mpvsocket'"</action>
                <action type="exit">Seite zurück</action>
            </button>
            <text>
                <label>" '$"Seite"' '$2' "</label>
            </text>
            <button tooltip-text="'$"nächste Seite"'">
                <variable>taste_seite-</variable>
                <label>"'$" → "'"</label>
                <action>echo "quit" | socat - "'$mpvsocket'"</action>
                <action type="exit">Seite vor</action>
            </button>
        </hbox>    ' >> "$temporaerdatei_01"
fi
h=0
while true; do
    g=0
    echo '        <hbox>    ' >> "$temporaerdatei_01"
    while [ $(($g+${c_len[$h]})) -le $l ]; do
        echo '            <button tooltip-text="'$"Ein- oder Umschalten zu Sender"" »${sender[$h]}«"'">
            <label> '${sender[$h]}' </label>
            <variable>taste_sender_'$h'</variable>
            <action>setsid /bin/bash $(if ! kill -0 $(cat "'$temporaerdatei_02'") 2>/dev/null 2>&1; then /bin/bash "'$temporaerdatei_07'"; fi) &</action>
            <action>while ! echo "{ \"command\": [\"get_property\", \"idle-active\"] }" | socat - "'$mpvsocket'" | grep -E "true|false" >/dev/null 2>&1; do sleep .5; done</action>
            <action>echo '$(printf "\x27")'{ "command": ["set_property", "screenshot-template", "'$"Szenenfoto"'-'${sender[$h]}'-%td.%tm.%tY-%tH:%tM:%tS"] }'$(printf "\x27")' | socat - "'$mpvsocket'"</action>
            <action>echo '$(printf "\x27")'{ "command": ["set_property", "screenshot-directory", "'$(printf "\x27")'$(sed -n "/^standbildspeicherpfad=/ s/standbildspeicherpfad=\x27\(..*\)\x27/\1/p" "'$einstellungsdatei'")'$(printf "\x27")'"] }'$(printf "\x27")' | socat - "'$mpvsocket'"</action>
            <action>echo '$(printf "\x27")'{ "command": ["set_property", "screenshot-format", "'$(printf "\x27")'$(sed -n "/^standbildformat=/ s/standbildformat=\x27\(..*\)\x27/\1/p" "'$einstellungsdatei'")'$(printf "\x27")'"] }'$(printf "\x27")' | socat - "'$mpvsocket'"</action>
            <action>if [ "'${sender[$h]}'" != "$(cat "'$temporaerdatei_08'")" ]; then echo "loadfile "'${adresse[$h]}'"" | socat - "'$mpvsocket'"; touch "'$temporaerdatei_11'"; fi</action>
            <action>echo '${adresse[$h]}' > "'$temporaerdatei_04'"</action>
            <action>echo "'${sender[$h]}'" > "'$temporaerdatei_08'"</action>
            <action>echo '$h' > "'$temporaerdatei_10'"</action>' >> "$temporaerdatei_01"

# Dirty workaround since even exported variables on runtime won't survive till next action tag and even
#                <action type="enable">taste_sender_$(cat "'$temporaerdatei_09'")</action>
# doesn't work in this cripled gtkdialog thing. So'we'll have to send the activation to all sender buttons instead.
        for (( m=0; m<$tastenanzahl; m++ ));
            do
                echo '                <action type="enable">taste_sender_'$m'</action>' >> "$temporaerdatei_01"
            done
        echo '                <action type="disable">taste_sender_'$h'</action>
            <action type="disable">taste_einstellungen</action>
            <action type="enable">taste_stop</action>
            <action type="enable">taste_schnappschuss</action>
            <action type="enable">taste_mitschnitt</action>
            <action type="disable">taste_listenwechsel</action>
            <action type="disable">taste_sender+</action>
            <action type="disable">taste_sender-</action>
            <action>xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")</action>
            <action>setsid nice -n 5 /bin/bash $(sleep 12; while true; do flag_running=false; n=1; while [ "$n" -le 5 ]; do if echo "{ \"command\": [\"get_property\", \"idle-active\"] }" | socat - "'$mpvsocket'" | grep -E "false" >/dev/null 2>&1; then flag_running=true; fi; sleep .2; n=$(( n + 1 )); done; if ! $flag_running; then break; fi; done; rm -f "'$temporaerdatei_11'"; if [ ! -e "'$temporaerdatei_05'" ]; then if ! kill -0 $(cat "'$temporaerdatei_02'") 2>/dev/null 2>&1; then xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); xdotool windowmap $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); /bin/bash "'$temporaerdatei_07'"; xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); xdotool windowmap $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); else echo "" > "'$temporaerdatei_08'"; if xdotool search --onlyvisible --name "'$"aCSTV antiX Community Simple TV Starter"'" >/dev/null; then xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); xdotool windowmap $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); else xdotool windowmap $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); fi; fi; fi) &</action>
            </button>            ' >> "$temporaerdatei_01"
        g=$(($g+${c_len[$h]}))
        let $((h++))
        [ -z ${c_len[$h]} ] && break
    done
    echo '        </hbox>        ' >> "$temporaerdatei_01"
    [ -z ${c_len[$h]} ] && break
done
echo '    </frame>
        <hbox>
            <button tooltip-text="'$"Aktuelle Programmvorschau im Browser zeigen"'">
                <label>"'$" Programmvorschau "'"</label>
                <action>bash -c "set -m; coproc xdg-open $informationen; disown $!"</action>
            </button>
            <button tooltip-text="'$"aCSTV Grundeinstellungen vornehmen"'">
                <label>"'$" Einstellungen "'"</label>
                <variable>taste_einstellungen</variable>
                <action>if ! xdotool search --name "aCSTV Einstellungen"; then /bin/bash "'$temporaerdatei_03'"; fi &</action>
            </button>
            <button tooltip-text="'$"Laufendes Programm ausschalten"'" sensitive="false">
                <label>"'$" Stop "'"</label>
                <variable>taste_stop</variable>
                <action>echo "stop" | socat - "'$mpvsocket'"</action>
                <action>rm -f "'$temporaerdatei_11'"</action>
                <action>echo "" > "'$temporaerdatei_08'"</action>
                <action type="enable">taste_einstellungen</action> ' >> "$temporaerdatei_01"
# Dirty workaround since even exported variables on runtime won't survive till next action tag and even
#                <action type="enable">taste_sender_$(cat "'$temporaerdatei_09'")</action>
# doesn't work in this cripled gtkdialog thing. So'we'll have to send the activation to all sender buttons instead.
            for (( m=0; m<$tastenanzahl; m++ ));
                do
                    echo '                <action type="enable">taste_sender_'$m'</action>' >> "$temporaerdatei_01"
                done
echo '                <action type="disable">taste_stop</action>
                <action type="disable">taste_schnappschuss</action>
                <action type="disable">taste_aufnahme_stop</action>
                <action type="disable">taste_mitschnitt</action>
                <action type="enable">taste_listenwechsel</action>
                <action type="enable">taste_sender+</action>
                <action type="enable">taste_sender-</action>
            </button>
            <button tooltip-text="'$"aCSTV verlassen"'">
                <label>"'$" Beenden "'"</label>
                <action>kill -15 "'$$'"</action>
                <action type="exit">aCSTV wird beendet</action>
            </button>
        </hbox>
        <hbox>
            <button tooltip-text="'$"Senderliste wechseln"'">
                <label>"  ⎘  "</label>
                <variable>taste_listenwechsel</variable>            
                <action>if [ ! -e "'$temporaerdatei_11'" ] && [ ! -e "'$temporaerdatei_05'" ]; then /bin/bash "'$temporaerdatei_18'"; fi</action>
            </button>
            <button tooltip-text="'$"Videomitschnitt der aktuellen Filmszene anfertigen"'" sensitive="false">
                <label>"'$" Videoaufnahme "'"</label>
                <variable>taste_mitschnitt</variable>
                <action>touch "'$temporaerdatei_05'"</action>
                <action>if [ "$(cat "'$temporaerdatei_08'")" != "" ]; echo "quit" | socat - "'$mpvsocket'"; then while $(kill -0 $(cat "'$temporaerdatei_02'" 2>/dev/null)); do sleep .5; done; sleep .5; /bin/bash "'$temporaerdatei_06'"; fi</action>
                <action type="enable">taste_aufnahme_stop</action>
                <action type="enable">taste_schnappschuss</action>
                <action type="disable">taste_listenwechsel</action>
                <action type="disable">taste_mitschnitt</action>
                <action type="disable">taste_einstellungen</action>
                <action type="disable">taste_stop</action>
                <action type="disable">taste_sender+</action>
                <action type="disable">taste_sender-</action>' >> "$temporaerdatei_01"

for (( i=0; i<$tastenanzahl; i++ ));
do
    echo '                <action type="disable">taste_sender_'$i'</action>' >> "$temporaerdatei_01"
done

echo '                <action>sleep 1; xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")</action>
            <action>setsid nice -n 5 /bin/bash $(sleep 12; while true; do flag_running=false; n=1; while [ "$n" -le 5 ]; do if echo "{ \"command\": [\"get_property\", \"idle-active\"] }" | socat - "'$mpvsocket'" | grep -E "false" >/dev/null 2>&1; then flag_running=true; fi; sleep .2; n=$(( n + 1 )); done; if ! $flag_running; then break; fi; done; if [ -e "'$temporaerdatei_05'" ]; then echo "{ \"command\": [\"quit\"] }" | socat - "'$mpvsocket'"; while kill -0 $(cat "'$temporaerdatei_02'") 2>/dev/null 2>&1; do sleep .5; done; rm -f "'$temporaerdatei_05'"; /bin/bash "'$temporaerdatei_07'"; xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); xdotool windowmap $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); fi) &</action>
            </button>
            <button tooltip-text="'$"Laufenden Videomitschnitt stoppen"'" sensitive="false">
                <label>"'$" Aufnahme Stop "'"</label>
                <variable>taste_aufnahme_stop</variable>
                <action>touch "'$temporaerdatei_11'"</action>
                <action>if [ -e "'$temporaerdatei_05'" ]; then rm -f "'$temporaerdatei_05'"; echo "stop" | socat - "'$mpvsocket'"; echo "quit" | socat - "'$mpvsocket'"; while $(kill -0 $(cat "'$temporaerdatei_02'")) 2>/dev/null; do sleep .5; done; sleep .5; /bin/bash "'$temporaerdatei_07'"; fi</action>
                <action>while ! echo "{ \"command\": [\"get_property\", \"idle-active\"] }" | socat - "'$mpvsocket'" | grep -E "true|false"; do sleep .5; done</action>
                <action>echo '$(printf "\x27")'{ "command": ["set_property", "screenshot-template", "'$"Szenenfoto"'-'$(printf "\x27")'$(cat "'$temporaerdatei_08'")'$(printf "\x27")'-%td.%tm.%tY-%tH:%tM:%tS"] }'$(printf "\x27")' | socat - "'$mpvsocket'"</action>
                <action>echo '$(printf "\x27")'{ "command": ["set_property", "screenshot-directory", "'$(printf "\x27")'$(sed -n "/^standbildspeicherpfad=/ s/standbildspeicherpfad=\x27\(..*\)\x27/\1/p" "'$einstellungsdatei'")'$(printf "\x27")'"] }'$(printf "\x27")' | socat - "'$mpvsocket'"</action>
                <action>echo '$(printf "\x27")'{ "command": ["set_property", "screenshot-format", "'$(printf "\x27")'$(sed -n "/^standbildformat=/ s/standbildformat=\x27\(..*\)\x27/\1/p" "'$einstellungsdatei'")'$(printf "\x27")'"] }'$(printf "\x27")' | socat - "'$mpvsocket'"</action>
                <action>echo "loadfile $(cat '$temporaerdatei_04')" | socat - "'$mpvsocket'"</action>
                <action type="disable">taste_aufnahme_stop</action>
                <action type="enable">taste_mitschnitt</action>
                <action type="enable">taste_stop</action>
                <action type="enable">taste_sender+</action>
                <action type="enable">taste_sender-</action>' >> "$temporaerdatei_01"

for (( i=0; i<$tastenanzahl; i++ ));
do
    echo '                <action type="enable">taste_sender_'$i'</action>' >> "$temporaerdatei_01"
done

echo '                <action type="disable">taste_sender_$(cat "'$temporaerdatei_10'")</action>
                <action>xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")</action>
                <action>setsid nice -n 5 /bin/bash $(sleep 12; while true; do flag_running=false; n=1; while [ "$n" -le 5 ]; do if echo "{ \"command\": [\"get_property\", \"idle-active\"] }" | socat - "'$mpvsocket'" | grep -E "false" >/dev/null 2>&1; then flag_running=true; fi; sleep .2; n=$(( n + 1 )); done; if ! $flag_running; then break; fi; done; if [ -e "'$temporaerdatei_11'" ]; then echo "{ \"command\": [\"quit\"] }" | socat - "'$mpvsocket'"; while kill -0 $(cat "'$temporaerdatei_02'") 2>/dev/null 2>&1; do sleep .5; done; rm -f "'$temporaerdatei_11'"; /bin/bash "'$temporaerdatei_07'"; xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); xdotool windowmap $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); fi) &</action>
            </button>
            <button tooltip-text="'$"Standbild der aktuellen Filmszene aufnehmen"'" sensitive="false">
                <label>"'$" Szenenfoto "'"</label>
                <variable>taste_schnappschuss</variable>
                <action type="disable">taste_schnappschuss</action>
                <action>if [ "$(cat "'$temporaerdatei_08'")" != "" ]; then dateiname="$(echo "'$speicherpfad_standbild'/""'$"Szenenfoto"'-$(cat "'$temporaerdatei_08'")-$(date +%d.%b.%Y-%H:%M:%S).$(sed -n "/^standbildformat=/ s/standbildformat=\x27\(..*\)\x27/\1/p" "'$einstellungsdatei'")")"; echo "screenshot-to-file $dateiname video" | socat - "'$mpvsocket'"; while [ ! -s $dateiname ]; do sleep .5; done ; { nice -n 20 feh "'$FehRahmen'" -. -Z -^ "'$"Szenenfoto-Vorschau"' — $dateiname" $dateiname & sleep 1 ; pv=25;ph=$(($(wmctrl -d | grep "*" | cut -d" " -f5 | cut -dx -f1)/2+50));grhn=$(($(wmctrl -d | grep "*" | cut -d" " -f5 | cut -dx -f1)/2-75)); grv=$(echo "{ \"command\": [\"get_property\", \"height\"] }" | socat - "$mpvsocket" | sed -n "s/^{\x22data\x22:\([[:digit:]][[:digit:]]*\),..*/\1/p"); grh=$(echo "{ \"command\": [\"get_property\", \"width\"] }" | socat - "$mpvsocket" | sed -n "s/^{\x22data\x22:\([[:digit:]][[:digit:]]*\),..*/\1/p"); grvn=$(($grhn*$grv/$grh)); xdowid=$(($(wmctrl -l | grep -F "'$"Szenenfoto-Vorschau"' — $dateiname" | cut -d" " -f1))); xdotool windowsize $xdowid $grhn $grvn windowmove $xdowid $ph $pv windowraise $xdowid; wmctrl -r "'$"Szenenfoto-Vorschau"' — $dateiname" -b add,above ; } fi</action>
                <action>sleep 1</action>
                <action type="enable">taste_schnappschuss</action>
            </button>
        </hbox>
    </vbox>
    <action signal="key-press-event" condition="command_is_true( [ $KEY_SYM = minus ] && echo true )">if xdotool search --onlyvisible --name "'$"aCSTV antiX Community Simple TV Starter"'"; then xdotool windowminimize $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); else xdotool windowmap $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'"); fi</action>
    <action signal="key-press-event" condition="command_is_true( [ $KEY_SYM = b ] && ( kill -15 '$$';  ) )" type="exit">"aCSTV wird beendet"</action>
    <action signal="map-event" condition="command_is_true( [ -e '$temporaerdatei_11' ] && echo true )">enable:taste_stop</action>
    <action signal="map-event" condition="command_is_true( [ -e '$temporaerdatei_11' ] || [ -e '$temporaerdatei_05' ] && echo true )">enable:taste_schnappschuss</action>
    <action signal="map-event" condition="command_is_true( [ -e '$temporaerdatei_11' ] && echo true )">enable:taste_mitschnitt</action>
    <action signal="map-event" condition="command_is_true( [ -e '$temporaerdatei_11' ] || [ -e '$temporaerdatei_05' ] && echo true )">disable:taste_einstellungen</action>
    <action signal="map-event" condition="command_is_true( [ -e '$temporaerdatei_11' ] || [ -e '$temporaerdatei_05' ] && echo true )">disable:taste_listenwechsel</action>
    <action signal="map-event" condition="command_is_true( [ -e '$temporaerdatei_11' ] || [ -e '$temporaerdatei_05' ] && echo true )">disable:taste_seite+</action>
    <action signal="map-event" condition="command_is_true( [ -e '$temporaerdatei_11' ] || [ -e '$temporaerdatei_05' ] && echo true )">disable:taste_seite-</action>
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && echo true )">disable:taste_stop</action>
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && [ ! -e '$temporaerdatei_05' ] && echo true )">disable:taste_schnappschuss</action>
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && echo true )">disable:taste_mitschnitt</action>    
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && [ ! -e '$temporaerdatei_05' ] && echo true )">enable:taste_einstellungen</action>
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && [ ! -e '$temporaerdatei_05' ] && echo true )">enable:taste_listenwechsel</action>
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && [ ! -e '$temporaerdatei_05' ] && echo true )">echo "" > "'$temporaerdatei_08'"</action>
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && [ ! -e '$temporaerdatei_05' ] && echo true )">enable:taste_seite+</action>    
    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && [ ! -e '$temporaerdatei_05' ] && echo true )">enable:taste_seite-</action>' >> "$temporaerdatei_01"

for (( i=0; i<$tastenanzahl; i++ ));
do
    echo '    <action signal="map-event" condition="command_is_true( [ ! -e '$temporaerdatei_11' ] && [ ! -e '$temporaerdatei_05' ] && echo true )">enable:taste_sender_'$i'</action>' >> "$temporaerdatei_01"
done

echo '</window>
' >> "$temporaerdatei_01"
chmod 755 "$temporaerdatei_01"

wid=""
while true; do
    wid=$(xdotool search --name $"aCSTV antiX Community Simple TV Starter")
    if [ "$wid" != "" ]; then
        sleep 1
        wmctrl -i -r $wid -b add,above
        break  
    fi
    sleep 1
done &
}

function senderliste_wechseln {
# (change stations list from main dialog)    
    cat <<<'#!/bin/bash
    acstv_wid=""
    while true; do
        acstv_wid=$(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
        if [ "$acstv_wid" != "" ]; then
            sleep .2
            xdotool windowunmap $acstv_wid
            break  
        fi
        sleep .2
    done &
    neue_senderliste="$(yad $YadRahmen --fixed --height=200 --width=200 \
        --center --borders=10 \
        --window-icon="'$symbol_aCSTV'" \
        --title="'$"aCSTV Senderliste wechseln"'" \
        --text="<b>""'$"aCSTV Senderliste wechseln"'""</b>\n\n""'$"Bitte wählen Sie die gewünschte Senderliste aus dem Menü."'""\n " \
        --form \
        --separator="|" \
        --item-separator="|" \
        --field="'$"Senderliste:"'":FL "$stationsliste" \
        --button="'$"Abbruch"'":5 --button="'$"Liste wechseln"'":4)"
    tastenauswahl=$?
    if [ $tastenauswahl -eq 4 ]; then
        xdotool windowclose $(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
        kill -15 $(cut -d- -f2 <<< "'"$temporaerdatei_01"'" | rev | cut -c 7- | rev)
        sleep .5     # give mpvsocket a small amount of time to be removed, otherwise mpv won’t create a fresh server socket on restart.
        setsid /bin/bash aCSTV.sh -s "${neue_senderliste::-1}" & exit 0
    elif [ $tastenauswahl -eq 5 ] || [ $tastenauswahl -eq 252 ]; then
        while true; do
            acstv_wid=$(xdotool search --name "'$"aCSTV antiX Community Simple TV Starter"'")
            if [ "$acstv_wid" != "" ]; then
                sleep .2
                xdotool windowmap $acstv_wid
                break  
            fi
            sleep .2
        done &
        exit 0
    fi
' > "$temporaerdatei_18"
chmod 755 "$temporaerdatei_18"
}

function aufnahmewarnung {
# recording warning icon in system status bar
    cat <<<'#!/bin/bash
wmctrl -c "'$"aCSTV Aufnahme"'" && exit 0
Speicherziel="$1"
Speicherdatei="$(rev <<< "$Speicherziel" | cut -d/ -f1 | rev)"
Speicherpfad="$(rev <<< "$Speicherziel" | cut -d/ -f2- | rev)"
d_sep="$(head -n1 <<<"$(locale LC_NUMERIC)")"

function Lesbares_Format {
# Umwandeln des Bytewertes in ein lesbares Format, Hervorheben bei Unterschreiten des Grenzwertes aus $2 (convert to human readable format and higlight if $2 is undercut)
exec 6>&1 1>&7- # schwatzhafte Befehle aus der Rückgabe fernhalten (keep console gossip from other commands out of returned string)
ha=""; hb=""; if [ $1 -le $2 ]; then ha="<span foreground='"'"'red'"'"'>"; hb="</span>"; fi

if [ $(bc <<<"$1 >= 2^100") -eq 1 ]; then
    a="$ha$(bc <<<"scale=4;$1/2^100" | tr "." "$d_sep")"" QiB$hb"
elif [ $(bc <<<"$1 >= 2^90") -eq 1 ]; then
    a="$ha$(bc <<<"scale=4;$1/2^90" | tr "." "$d_sep")"" RiB$hb"
elif [ $(bc <<<"$1 >= 2^80") -eq 1 ]; then
    a="$ha$(bc <<<"scale=4;$1/2^80" | tr "." "$d_sep")"" YiB$hb"
elif [ $(bc <<<"$1 >= 2^70") -eq 1 ]; then
    a="$ha$(bc <<<"scale=4;$1/2^70" | tr "." "$d_sep")"" ZiB$hb"
elif [ $(bc <<<"$1 >= 2^60") -eq 1 ]; then
    a="$ha$(bc <<<"scale=4;$1/2^60" | tr "." "$d_sep")"" EiB$hb"
elif [ $1 -ge $((2**50)) ]; then   # gerade noch berechenbar mit plain bash (bash arithmetic expansion integers can calculate that still.)
    a="$ha$(bc <<<"scale=3;$1/2^50" | tr "." "$d_sep")"" PiB$hb"
elif [ $1 -ge $((2**40)) ]; then
    a="$ha$(bc <<<"scale=2;$1/2^40" | tr "." "$d_sep")"" TiB$hb" # Heutige Laufwerksgrößen beginnen hier (above drive sizes maybe used in future days...)
elif [ $1 -ge $((2**30)) ]; then
    a="$ha$(bc <<<"scale=1;$1/2^30" | tr "." "$d_sep")"" GiB$hb"
elif [ $1 -ge $((2**20)) ]; then
    a="$ha$(bc <<<"scale=0;$1/2^20" | tr "." "$d_sep")"" MiB$hb"
elif [ $1 -ge 1024 ]; then
    a="$ha$(bc <<<"scale=0;$1/1024") KiB$hb"
else
    a="$ha$1"" Bytes$hb"
fi
echo $a >&6
}

function Zeit_lesbar {
# (convert time to human readable format)
    exec 6>&1 1>&7-
    ha=""; hb=""; if [ $1 -le $2 ]; then ha="<span foreground='"'"'red'"'"'>"; hb="</span>"; fi
    if [ $(bc <<<"$1 > 2*8760*60^2") -eq 1 ]; then
        a="'$"ca."'"" $(bc <<<"$1/(8760*(60^2))") ""'$"Jahren"'"
    elif [ $(bc <<<"$1 > 1440*60^2") -eq 1 ]; then
        a="'$"ca."'"" $(bc <<<"$1/(1440*(60^2))") ""'$"Monaten"'"
    elif [ $(bc <<<"$1 > 48*60^2") -eq 1 ]; then
        a="'$"ca."'"" $(bc <<<"$1/(24*(60^2))") ""'$"Tagen"'"
    elif [ $(bc <<<"$1 >= 60^2") -eq 1 ]; then
        a="'$"ca."'"" $(bc <<<"a=$1/60^2;b=($1/60)%60;if(b<10)print a,\":0\",b else print a,\":\",b") ""'$"Std."'"
    elif [ $(bc <<<"$1 >= 60") -eq 1 ]; then
        a="'$"ca."'"" $ha$(bc <<<"$1/60") ""'$"Min."'""$hb"
    else
        a="$ha$1 ""'$"Sek."'""$hb"
    fi
echo $a >&6
}

function Datenrate_lesbar {
# (convert data rate to human readable format)
    exec 6>&1 1>&7-
    if [ $1 -ge 1000 ]; then
      a="$(bc <<<"scale=2;$1/1000" | tr "." "$d_sep") kBit/"'$"Sek."'""
    else
      a="$1 Bit/"'$"Sek."'""
    fi
    echo $a >&6
}

exec 7>&1
gs=$(($(stat --format="%b*%B" "$Speicherziel")))   # Dateigröße in Byte (als Formel)  (file size formula)
gl="$(Lesbares_Format $gs 0)"                      # Dateigröße in lesbarem Format (Binärpräfix)  (file size human readable, binary prefix)
rs=$(($(stat -f --format="%a*%S" "$Speicherpfad")))   # Restspeicher in Byte (als Formel)   (remaining storage formula)
rl="$(Lesbares_Format $rs $((100*2**20)))"            # Restspeicher in lesbarem Format (Binärpräfix))  (remaining storage human readable, binary prefix)
p=$(stat -c "%W" "$Speicherziel")                     # Zeitpunkt der Dateierstellung
[ $p -eq 0 ] && p=$(stat -c "%X" "$Speicherziel")     # Workaround für Live-Dateisystem: Attribut "birth" nicht verfügbar
o=$(bc <<<"$gs/($(date +%s) - $p)")   # Datenrate in Byte/Sek. (data rate)
ol=$(Datenrate_lesbar $((8*$o)))    # Datenrate in lesbarem Format als Bit/Sek (SI-Präfix)  (data rate human readable, SI-prefix
rql=$(Zeit_lesbar $(bc <<<"$rs/$o") 1800)  # Restzeit in lesbarem Format   (max remaining recording time, human readable)
exec 7>&-

yad \
   --borders=15 $YadRahmen --fixed --posx=25  --posy=$(bc <<<"scale=0; $dy*5.2/10")  --skip-taskbar --on-top \
   --width=490 --height=300 \
   --window-icon="$symbol_aufnahme" \
   --title="'$"aCSTV Aufnahme"'" \
   --text="\t<b>""'$"aCSTV Aufnahme"'""</b>\n\n\t""'$"Der Mitschnitt der laufenden Sendung wird in der Datei"'""\n\t    <i>$Speicherdatei</i>\n\t""'$"im Verzeichnis"'""\n\t    <i>$Speicherpfad</i>\n\t""'$"gespeichert. "$"Gegenwärtige Dateigröße:"'""  $gl""\n\t""'$"Der verfügbare Speicherplatz von"'"" $rl ""'$"reicht bei der\n\taktuellen durchschnittlichen Datenrate von"'"" $ol\n\t""'$"näherungsweise für eine Aufzeichnung von"'"" $rql\n\n\t""'$"Zum Beenden der Aufnahme drücken Sie bitte im\n\taCSTV Hauptfenster die Taste <i>»Aufnahme Stop«</i>."'""\n " \
   --image="/usr/share/icons/hicolor/96x96/apps/aCSTV2.png" \
   --button=$"OK":0 &
wid_aufnahme=""
until [ "$wid_aufnahme" != "" ]; do wid_aufnahme=$(xdotool search --name "'$"aCSTV Aufnahme"'"); done
xdotool windowmove $wid_aufnahme 25 $(bc <<<"scale=0; $dy*5.2/10")   # dirty workaround: yad berechnet die Fenstergröße falsch und setzt daher die Fensterposition ebenfalls falsch.
' > "$temporaerdatei_19"
chmod 755 "$temporaerdatei_19"
}

function desktopgröße {
export dx=$(wmctrl -d | tr -s " " | cut -d" " -f-4 | grep -F "*" | cut -d" " -f4 | cut -dx -f1)
export dy=$(wmctrl -d | tr -s " " | cut -d" " -f-4 | grep -F "*" | cut -d" " -f4 | cut -dx -f2)
}

# Script startet hier (here starts script actually)
ländereinstellung
programmhilfe
einstellungen_lesen
tastenbefehle  # reactivated, since static input.conf in private mpv config folder must be updataed with current pid on each run
mpv_starten
aufnahme
aufnahmestop
aufnahmewarnung
einstellungen_ändern
stationsliste_bearbeiten
stationsliste_einlesen 1
if [ $? -ne 0 ]; then
    yad --width=300 --height=120 --fixed --center $YadRahmen --borders=10 \
    --window-icon="$symbol_fehler" \
    --title=$"aCSTV Senderliste – Fehler" \
    --text=$"Stationsliste enthält keine aktiven Einträge.\nBitte aktivieren Sie mindestens einen Eintrag\noder fügen einen gültigen Eintrag hinzu." \
    --button=$"Einträge bearbeiten":0
    /bin/bash "$temporaerdatei_12"
    if [ $? -eq 6 ] ; then
        yad --width=300 --height=120 --fixed --center $YadRahmen --borders=10 \
        --window-icon="$symbol_fehler" \
        --title=$"aCSTV Senderliste – Fehler" \
        --text=$"Stationsliste ohne gültige Einträge.\nAbbruch auf Nuztzerwunsch." \
        --button=$"aCSTV beenden":0
        exit 0
    fi
fi
stationsliste_aktualisieren
senderliste_wechseln
dialogaufteilung
verbindungstest
desktopgröße

eintraege=$(grep -c -v "^#" "$stationsliste")
seiten=$((eintraege/60))
[ $((seiten*60)) -lt $eintraege ] && let $((seiten++))
seite=1
while :; do
    hauptdialog $seiten $seite
    ergebnis_gtkdialog=$(gtkdialog -f "$temporaerdatei_01" -G "+$(bc <<<"scale=0; $dx*5.7/10")+$(bc <<<"scale=0; $dy*5.2/10")" | grep -F "EXIT=" | cut -d= -f2 | tr -d '"')
    if [ "$ergebnis_gtkdialog" == "Seite zurück" ]; then
        if [ $seite = 1 ]; then seite=$seiten; else let $((seite--)); fi
        stationsliste_einlesen $seite
        dialogaufteilung
    elif [ "$ergebnis_gtkdialog" == "Seite vor" ]; then
        if [ $seite = $seiten ]; then seite=1; else let $((seite++)); fi
        stationsliste_einlesen $seite
        dialogaufteilung
    else
        break
    fi
done

exit 0


# script logic:
# -------------

# right at startup mpv ist started in idle mode with the settings from the settings file and sender listfile stored. The language and country specific 
# translation- help- and stations list files are copied from resource directory as working copys to user's home folder, depending on the user's locale settings.
# Most time there is no need to restart mpv, all commands should get filed to it using its IPC connection. Also the button field is minimised.
# In playback mode: Every time a channel button is pressed, the respective url is transmitted to mpv via socat. At the same time the sender number, its name
# and its url is written to tempfiles. This workaround is needed to make the information available in other gtkdialog "action" commands.
# Once another butten is pressed, the new URL is sent to mpv via IPC, and the three information files are rewritten. If stop button is pressed, the stop
# command is sent to mpv, and so on. On exit all temp files get deleted, mpv quit and all corresponding gtk and yad windows are closed.
# There is one exception, which wouldn't work using IPC command via socat: Starting and stopping a record. This task requires mpv to get restarted, so
# this renders it necessary to have two additional functions, one for starting a record, and another for stopping it, featuring the commands needed to restart
# the whole mpv instance. This behaviour of MPV makes this script unnecessary difficult in structure, since additional checks are needed wheter the instance is
# running or not and wheher it is in record mode or not. Also stopping the stream from outside (e.g. by closing mpv from task bar icon or from its shortcuts) is
# cought and the button window restored.
# Since q will leave mpv by default, we have to check redundantly whether it is still running when sending a new URL via IPC server connection.

# In each mode all buttens providing functions not applicable in the very mode are deactivated.

# While mpv is playing the videostream from URL, in intervals it is asked whether it is still playing back or whether it is idle-active (e.g. because of
# 500/404/403 errors from streaming server. As soon as it answers "true", the buttons are reset to initial setting and the main dialog window is shown on
# screen to chose next action. To make these constant requests not disturb any other running tasks, they are running niced to lowest priority.

# screenshot function can be used in playing mode and recording mode both. It uses the information from settings files and tempfiles to name the screenshot
# and determine the directory where it is do be stored. This command is sent to mpv via IPC.

# Program information URL will be opened in default browser.
